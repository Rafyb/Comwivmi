#Sprint 25/03  10h - 11h30

Florentin PLAISANT : S'inscrire à un evenement  ( web )
Jimmy LETORET : filtre complexe (web)
Julien VANRYSSEL : Recherche/filtre et inscription evenement ( mobile ) 
Manoel LECOCQ : Integration HTML/CSS sur la page de bienvenue et sur le header
Nathan FORESTIER : finir toutes les fonctionalités du serveur 
Raphael BAUVIN : Aider pour bien integrer tout les elements et Lister les evenment où l'utilisateur est inscris ( web )

##User story complété :
WEB
-> L'utilisateur arrive sur une page d'acceuil avec une description
-> L'utilisateur peut filtrer ses recherches avec plusieurs filtres en meme temps
MOBILE
-> L'utilisateur peut filtrer ses recherches avec plusieurs filtres en meme temps

##Bilan :
Il nous reste un après midi pour terminer les fonctionalités principales, nous allons donc nous contrer sur l'inscritipn à l'évenement et la suppresion 


##Objectif prochain sprint :
Lister evenement sur le profil ( web ), inscription à un evenement ( mobile ), supprimer activité ( web )