package comwivme.dao;

import java.util.Objects;

import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import comwivme.dto.SeraPresentDto;
import comwivme.dao.SeraPresentEntity;

@Entity
@Cacheable(false)
@Table(name = "serapresent")

@NamedQueries({
	@NamedQuery(name="FindSeraPresentById", query="SELECT p from SeraPresentEntity p where p.iduser = :pid"),
	@NamedQuery(name="FindSeraPresentByIdEvent", query="SELECT p from SeraPresentEntity p where p.idevent = :pid")

}
		)

public class SeraPresentEntity extends SeraPresentDto {

	private final static Logger logger = LoggerFactory.getLogger(SeraPresentEntity.class);
	private static ModelMapper modelMapper = new ModelMapper();
	
	public SeraPresentEntity(SeraPresentDto serapresent) {
		modelMapper.map(serapresent, this.getClass());
	}

	public SeraPresentEntity() { } 

	public static SeraPresentDto convertToDto(SeraPresentEntity serapresent) {
		return  modelMapper.map(serapresent, SeraPresentDto.class);
	}


	@Id
	@Column(name = "idevent")
	public long getIdevent() {
		return idevent;
	}
	public void setIdevent(long idevent) {
		this.idevent = idevent;
	}
	

	@Id
	@Column(name = "iduser")
	public long getIduser() {
		return iduser;
	}
	public void setIduser(long iduser) {
		this.iduser = iduser;
	}


	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		SeraPresentEntity that = (SeraPresentEntity) o;
		return iduser == that.iduser &&
				Objects.equals(iduser, that.iduser);
	}

	@Override
	public int hashCode() {
		return Objects.hash(iduser, idevent);
	}

	@Override
	public String toString() { //TODO: a finir
		return "SeraPresentDto [iduser=" + iduser + ", idevent	=" + idevent + "]";
	}
}
