// @flow
import Page from './pages/Page.js';
import $ from 'jquery';
import loginPage from './pages/login.js';

export default class SignInPage extends Page {
	constructor(){
		super('.content');
		// $FlowFixMe
		this.submit = this.submit.bind(this);
		this.login = new loginPage();
	}

	render():string {
		return `
<div class="container-login"><form class="signInPage">
		<label>
			Nom :
			<input type="text" name="nom" class="form-control">
		</label>
		<label>
			Prenom :
			<input type="text" name="prenom" class="form-control">
		</label>
		<label>
			Pseudo :
			<input type="text" name="pseudo" class="form-control">
		</label>
		<label>
			Email :
			<input type="text" name="email" class="form-control">
		</label>
		<label>
			Mot de Passe :
			<input type="password" name="passwd" class="form-control">
		</label>
		<label>
			Ville :
			<input type="text" name="ville" class="form-control" >
		</label>
		<label>
			Date de naissance :
			<input type="date" name="birth_date" class="form-control" >
		</label>
		
		<label>
			Centre d'interets :
			<select name="interets" multiple="true" class="form-control">
			<option disabled><b>Arts</b></option>
			<option value="Chant">Chant</option>
			<option value="Dessin">Dessin</option>
			<option value="Photo">Photo</option>

			<option disabled><b>Evenement</b></option>
			<option value="Anniversaire">Anniversaire</option>
			<option value="Concert">Concert</option>
			<option value="Spectacle">Spectacle</option>

			<option disabled><b>Jeux</b></option>
			<option value="Jdr">Jeux de rôle</option>
			<option value="Jeux-video">Jeux Video</option>
			<option value="Table-game">Jeux sur table</option>

			<option disabled><b>Voyages</b></option>
			<option value="Camping">Camping</option>
			<option value="Sejours">Sejours</option>
			<option value="Week-End">Week-End</option>


		</select>
		</label>
		<button type="submit" class="btn btn-default">Ajouter</button>
	</form></div>`;
	}

	mount(container:HTMLElement):void {
		$('form.signInPage').submit( this.submit );
	}

	submit(event:Event):void {
		event.preventDefault();
		const fieldNames:Array<string> = [
			'nom',
			'prenom',
			'pseudo',
			'email',
			'passwd',
			'birth_date',
			'ville'
		];
		// on récupère la valeur saisie dans chaque champ
		const values:any = {};
		const errors:Array<string> = [];

		fieldNames.forEach( (fieldName:string) => {
			const value = this.getFieldValue(fieldName);
			if ( !value ){
				errors.push( `Le champ ${fieldName} ne peut être vide !` );
			}
			values[fieldName] = value;
		});
		if (errors.length) {
			// si des erreurs sont détectées, on les affiche
			alert( errors.join('\n') );
		} else {
			// si il n'y a pas d'erreur on envoie les données
			const user = {
				date_naissance: values.birth_date+"T00:00:00",
				nom: values.nom,
				prenom: values.prenom,
				pseudo: values.pseudo,
				mail: values.email,
				ville: values.ville,
				password: values.passwd,
				role:"U"
			};
			console.log(user);
			fetch( '/comwivme/users', {
					method:'POST',
					headers: { 'Content-Type': 'application/json' },
					body: JSON.stringify(user)
				})
			.then(response => {
				if (!response.ok) {
					throw new Error( `${response.status} : ${response.statusText}` );
				}
				return response.json();
			})
			.then ( user => {
				alert(`Inscription "${user.nom}" enregistrée avec succès ! (id ${user.id})`);
				// puis on vide le formulaire
				const form:?HTMLElement = document.querySelector('form.signInPage');
				if (form && form instanceof HTMLFormElement) {
					form.reset();
				}
				PageRenderer.renderPage(this.login);
			})
			.catch( error => alert(`Enregistrement impossible : ${error.message}`) );
		}
	}

	getFieldValue(fieldName:string):?string|Array<string>{
		// on récupère une référence vers le champ qui a comme attribut `name` la valeur fieldName (nom, base, prix_petite, etc.)
		const field:?HTMLElement = document.querySelector(`[name=${fieldName}]`);
		if ( field instanceof HTMLInputElement ) {
			// s'il s'agit d'un <input> on utilise la propriété `value`
			// et on retourne la chaine de caractère saisie
			return field.value != '' ? field.value : null;
		} else if ( field instanceof HTMLSelectElement ) {
			// s'il s'agit d'un <select> on utilise la propriété `selectedOptions`
			const values:Array<string> = [];
			for (let i = 0; i < field.selectedOptions.length; i++) {
				values.push( field.selectedOptions[i].value );
			}
			// et on retourne un tableau avec les valeurs sélectionnées
			return values.length ? values : null;
		}
		return null;
	}
}