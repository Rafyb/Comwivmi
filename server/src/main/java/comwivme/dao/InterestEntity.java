package comwivme.dao;

import java.util.Objects;

import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import comwivme.dto.InterestDto;
import comwivme.dao.InterestEntity;

@Entity
@Cacheable(false)
@Table(name = "centresinteretsusers")

@NamedQueries({
	@NamedQuery(name="FindInterestsById", query="SELECT p from InterestEntity p where p.pseudo = :ppseudo"),
}
		)

public class InterestEntity extends InterestDto {

	private final static Logger logger = LoggerFactory.getLogger(InterestEntity.class);
	private static ModelMapper modelMapper = new ModelMapper();

	public InterestEntity(InterestDto interestDto) {
		modelMapper.map(interestDto, this.getClass());
	}

	public InterestEntity() { } 

	public static InterestDto convertToDto(InterestEntity interest) {
		return  modelMapper.map(interest, InterestDto.class);
	}


	@Id
	@Column(name = "nom")
	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}
	
	
	@Basic
	@Column(name = "pseudo")
	public String getpseudo() {
		return pseudo;
	}
	public void setpseudo(String pseudo) {
		this.pseudo= pseudo;
	}


	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		InterestEntity that = (InterestEntity) o;
		return nom == that.nom &&
				Objects.equals(nom, that.nom);
	}

	@Override
	public int hashCode() {
		return Objects.hash(pseudo, nom);
	}

	@Override
	public String toString() { //TODO: a finir
		return "EventDto [pseudo=" + pseudo+ ", nom=" + nom + "]";
	}
}
