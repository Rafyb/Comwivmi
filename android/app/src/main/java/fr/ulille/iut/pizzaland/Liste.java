package fr.ulille.iut.pizzaland;

public class Liste {

    String centre_interet;
    String description_evenement;
    String titre_evenement;
    String date_evenement;

    Liste(String interet, String description, String titre, String date){
        this.centre_interet = interet;
        this.description_evenement = description;
        this.titre_evenement = titre;
        this.date_evenement = date;
    }

    public String getCentre_interet(){
        return this.centre_interet;
    }

    public String getDescription_evenement(){
        return this.description_evenement;
    }

    public String getTitre_evenement(){
        return titre_evenement;
    }

    public String getDate_evenement(){
        return date_evenement;
    }
}
