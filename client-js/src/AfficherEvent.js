// @flow
import PageRenderer from './PageRenderer.js';
import ActiviteThumbnail from './ActiviteThumbnail.js';

PageRenderer.titleElement = document.querySelector('.pageTitle');
PageRenderer.contentElement = document.querySelector('.pizzasContainer');

let activites:Array<{nature:string, date:date, lieu:string, nbMax:number, desc:string, id:number, idOwner:number, idParticipant: Array<{id:number}>}> = [
	{ nature: 'Foot', date: new Date('December 17, 2019 03:24:00'), lieu: 'Lille', nbMax: 10, desc: 'Foot entre amis', id: 1, idOwner:1, idParticipant: new Array(1,2,3,4)},
	{ nature: 'Cinéma', date: new Date('December 1, 2019 17:00:00'), lieu: 'Roubaix', nbMax: 5, desc: 'Le roi Lion', id: 2, idOwner:2, idParticipant: new Array(1,2,3,4)},
	{ nature: 'Bar', date: new Date('April 5, 2019 10:30:00'), lieu: 'Lens', nbMax: 15, desc: 'Viens boire un coup avec nous !', id: 3, idOwner:3, idParticipant: new Array(1,2,3,4)},
	{ nature: 'Foot', date: new Date('December 17, 2019 03:24:00'), lieu: 'Lille', nbMax: 10, desc: 'Foot entre amis', id: 1, idOwner:1, idParticipant: new Array(1,2,3,4)},
	{ nature: 'Cinéma', date: new Date('December 1, 2019 17:00:00'), lieu: 'Roubaix', nbMax: 5, desc: 'Le roi Lion', id: 2, idOwner:2, idParticipant: new Array(1,2,3,4)},
	{ nature: 'Bar', date: new Date('April 5, 2019 10:30:00'), lieu: 'Lens', nbMax: 15, desc: 'Viens boire un coup avec nous !', id: 3, idOwner:3, idParticipant: new Array(1,2,3,4)},
	{ nature: 'Foot', date: new Date('December 17, 2019 03:24:00'), lieu: 'Lille', nbMax: 10, desc: 'Foot entre amis', id: 1, idOwner:1, idParticipant: new Array(1,2,3,4)},
	{ nature: 'Cinéma', date: new Date('December 1, 2019 17:00:00'), lieu: 'Roubaix', nbMax: 5, desc: 'Le roi Lion', id: 2, idOwner:2, idParticipant: new Array(1,2,3,4)},
	{ nature: 'Bar', date: new Date('April 5, 2019 10:30:00'), lieu: 'Lens', nbMax: 15, desc: 'Viens boire un coup avec nous !', id: 3, idOwner:3, idParticipant: new Array(1,2,3,4)},
	{ nature: 'Foot', date: new Date('December 17, 2019 03:24:00'), lieu: 'Lille', nbMax: 10, desc: 'Foot entre amis', id: 1, idOwner:1, idParticipant: new Array(1,2,3,4)},
	{ nature: 'Cinéma', date: new Date('December 1, 2019 17:00:00'), lieu: 'Roubaix', nbMax: 5, desc: 'Le roi Lion', id: 2, idOwner:2, idParticipant: new Array(1,2,3,4)},
	{ nature: 'Bar', date: new Date('April 5, 2019 10:30:00'), lieu: 'Lens', nbMax: 15, desc: 'Viens boire un coup avec nous !', id: 3, idOwner:3, idParticipant: new Array(1,2,3,4)}
];


let nbActivitesPerPage = 5;
let nbActivites = 0;
let idx = 0;
 rendu += '<div class ="container-listActivity">';
activites.forEach(element => {
	if(idx<nbActivitesPerPage) {
       
		let activ = new ActiviteThumbnail(element);
		rendu += activ.render();
		idx++;
	}
	nbActivites++;
    youlou youlàou
});
let nbPage = nbActivites/nbActivitesPerPage;
if(nbPage>nbPage.toFixed(0)){
	nbPage = Number(nbPage.toFixed(0)) + 1;
}
rendu += '</div>';
rendu += `<p>Page 1 / ${nbPage}</p>`;
const contenu:?HTMLElement = document.querySelector('.content');
contenu.innerHTML = rendu;

/*// @flow
import Popup from './Popup.js';
import $ from 'jquery';



const link:?HTMLElement = document.querySelector('.title');


fetch('http://localhost:8080/api/v1/pizzas')
    .then( (response:Response) => response.json() )
	.then( link.innerHTML = "Hello world")
	.then(console.log()
	.then(console.log("Connexion rest reussi")));


/** 
const scores:Array<{score:number, name:string}> = [
	{ score: 1, name: 'Brice' },
	{ score: 0.5, name: 'Thomas' },
	{ score: 0.5, name: 'Clément' },
	{ score: 0.25, name: 'Alexis' },
	{ score: 0.25, name: 'Anthony' },
	{ score: 0.25, name: 'Elric' },
	{ score: 0.25, name: 'Axelle' },
	{ score: 0.25, name: 'Romain' },
	{ score: 0.25, name: 'Titouan' },
	{ score: 0.25, name: 'Arnaud' },
	{ score: 0.25, name: 'Manon' },
	{ score: 0.25, name: 'Nicolas' },
	{ score: 0.25, name: 'Pierre' },
	{ score: 0, name: 'Les autres' }
];
//Fonction generant le classement en HTML 
function renderClassement(tab){
	let cpt = 1;
	let tableau:string = "";
	tab.forEach( function(tab) {
		if(old_score == tab.score){
			if(cpt < 4) {
				tableau+=`<li class='podium'><em ></em><strong>${tab.score}/20</strong><span>${tab.name}</span></li>`
			}else{
				tableau+=`<li class='other'><em></em><strong>${tab.score}/20</strong><span>${tab.name}</span></li>`
			}
			cpt++;
		}else{
			if(cpt < 4) {
				tableau+=`<li class='podium'><em >${cpt++}</em><strong>${tab.score}/20</strong><span>${tab.name}</span></li>`
			}else{
				tableau+=`<li class='other'><em>${cpt++}</em><strong>${tab.score}/20</strong><span>${tab.name}</span></li>`
			}
	}
		old_score = tab.score;
	});
	const scoreContainer:?HTMLElement = document.querySelector('.scoresContainer');
	scoreContainer.innerHTML = tableau;
}

//Variables contenant les Element HTML utile pour les events
const link:?HTMLElement = document.querySelector('footer');
const close:?HTMLElement = document.querySelector('.closeButton');
const buttonPodium:?HTMLElement = document.querySelector('.showPodium');
const buttonAll:?HTMLElement = document.querySelector('.showFullList');
const refreshButton:?HTMLElement = document.querySelector('.refreshButton');


const popup = new Popup('.popup');

renderClassement(scores);

function openPopup( event:Event ) {
	popup.open();
}

//Fonction mettant a jour le classment en utilsant AJAX et le JSON
function update(event:Event){
	console.log("Un rafraichissement ? ")
	$('.content').addClass("is-loading");
	fetch('./api/scores.json')
    .then( (response:Response) => response.json() )
	.then( renderClassement );
	$('.content').removeClass("is-loading");

}
//Fonction fermant la popup
function closePopup( event:Event ) {
	popup.close();
}
//Fonction cachant ce qui ne font pas parti du podium 
function onlyShowPodium(event:Event){
	$('.other').hide();
	console.log("COUCOU");
}

//Fonction montrant ce qui ne font pas parti du podium
function showAll(event:Event){
	$('.other').show();
	console.log("BEU");

}


if(refreshButton){
	refreshButton.addEventListener('click', update);
}
if(buttonPodium){
	buttonPodium.addEventListener('click', onlyShowPodium);
}
if(buttonAll){
	buttonAll.addEventListener('click', showAll);
}
if(link){
	link.addEventListener('click', openPopup);
}
if(close){
	close.addEventListener('click',closePopup);
}
let old_score = 0;
let visible= `style="visibility:none"`;

*/
