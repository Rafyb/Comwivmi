package fr.ulille.iut.pizzaland;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

public class MainActivity extends AppCompatActivity {
    private static String base_uri;
    public static final String VOLLEY_TAG = "TEST_PIZZALAND";
    public static final String LOG_TAG = "APPLI";

    private RequestQueue queue;

    private TextView tvDisplay;
    private CheckBox cbIutMain;
    private EditText etServeur;
    private CheckBox cbGetPizzas;
    private CheckBox cbPostIngredient;
    private CheckBox cbGetIngredient;
    private CheckBox cbGetIngredientsAsDto;
    private CheckBox cbGetOnePizzas;

    public static final String SERVER_KEY = "SERVEUR";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_firstpage);
    }

    public void doSuivant(View view){

        Log.d("doConnexion", " Passe dans doConnexion");
        startActivity(new Intent(this, Page_Description.class));
    }

    public void doPasser(View view){

        startActivity(new Intent(this, Connexion.class));
    }


}
