#Sprint 25/03  8h - 9h30

Florentin PLAISANT : S'inscrire à un evenement  ( web )
Jimmy LETORET : Lister les evenment où l'utilisateur est inscris ( web )
Julien VANRYSSEL : Recherche/filtre et inscription evenement ( mobile ) 
Manoel LECOCQ : Integration HTML/CSS sur la page de bienvenue et sur le header
Nathan FORESTIER : Gerer les liens Utilisateurs / Evenement ( serveur )
Raphael BAUVIN : Aider pour bien integrer tout les elements ensemble et erreur serveur

##User story complété :
-> L'interface est beaucoup plus asthétique

##Bilan :
Nous avons commencé à integrer le CSS et nous avançons sur la gestion des evenements

##Objectif prochain sprint :
Continuer d'integrer l'apparence web, S'inscrire à un evenement  ( web ), Recherche/filtre et inscription evenement ( mobile ),  Lister les evenment où l'utilisateur est inscris ( web )