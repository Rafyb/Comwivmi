package fr.ulille.iut.pizzaland;

import android.os.Bundle;
import android.widget.TextView;

import java.io.Serializable;
import java.util.Date;

public class User implements Serializable {

    String Nom;
    String Prenom;
    String Email;
    String Mdp;
    String Ville;
    String Date;
    String Pseudo;

    User(String nom, String prenom, String email, String mdp, String ville, String date, String pseudo){

        this.Nom = nom;
        this.Prenom = prenom;
        this.Email = email;
        this.Mdp = mdp;
        this.Ville = ville;
        this.Date = date;
        this.Pseudo = pseudo;
    }

    public String getNom(){
        return Nom;
    }

    public String getPrenom(){
        return Prenom;
    }

    public String getEmail(){
        return Email;
    }

    public String getMdp(){
        return Mdp;
    }

    public String getVille(){
        return Ville;
    }

    public String getDate(){
        return Date;
    }

    public String getPseudo() { return Pseudo;}
}
