package comwivme.dao;


import javax.persistence.*;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import comwivme.dto.UserDto;

import java.time.LocalDateTime;
import java.util.*;

@Entity
@Table(name = "user")

@NamedQueries({
    @NamedQuery(name="FindAllUsers", query="SELECT p from UserEntity p"),
    @NamedQuery(name="CheckUserName", query="SELECT count(p) from UserEntity p where p.id = :pid and p.id <> :pid"),
    @NamedQuery(name="FindUserByPseudo", query="SELECT p from UserEntity p where p.pseudo = :ppseudo"), //TODO: ameliorer ça
    @NamedQuery(name="FindUserById", query="SELECT p from UserEntity p where p.id = :pid"),
    @NamedQuery(name="VerifUser", query="SELECT p from UserEntity p where p.mail = :mail AND p.password = :password"), 
})

public class UserEntity extends UserDto {
    private final static Logger logger = LoggerFactory.getLogger(UserEntity.class);
    private static ModelMapper modelMapper = new ModelMapper();

    public static  UserEntity convertFromUserDto(UserDto user) {
        return modelMapper.map(user, UserEntity.class);
    }

    public UserEntity() {}
    
    public static UserDto convertToDto(UserEntity user) {
		return  modelMapper.map(user, UserDto.class);
	}
	

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
	public long getId() {
        return id;
    }

	public void setId(long id) {
        this.id = id;
    }
    
    @Basic
    @Column(name = "pseudo", nullable = false, length = -1)
	public String getPseudo() {
        return pseudo;
    }

	public void setPseudo(String pseudo) {
        this.pseudo = pseudo;
    }
	
	@Basic
	@Column(name = "mail")
	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}
	
	@Basic
	@Column(name = "password")
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	@Basic
	@Column(name = "nom")
	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}
	
	@Basic
	@Column(name = "prenom")
	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	
	public String toString() {
		String re = "{\"id\":" + id + ", \"pseudo\":\"" + pseudo + "\",\"mail\":\""+mail+"\",\"nom\":\""+nom+"\",\"prenom\":\""+prenom+"\",\"date_naissance\":\""+
	date_naissance+"\",\"ville\":\""+ville+"\",\"role\":\""+role+"\"}";
		System.out.println("2222"+re);
		//'{"date":"2001-09-24T00:00:00","nom":"Nord","prenom":"Paul","pseudo":"Polo","email":"polo@gmail.com",
	    // "ville":"Lille","interets":[""],"password":"pwd","role":"U"}' 
		return re;
		
	}
	
	@Basic
	@Column(name = "date_naissance")
	public LocalDateTime getDate_naissance() {
		return date_naissance;
	}

	public void setDate_naissance(LocalDateTime date) {
		this.date_naissance = date;
	}
	
	@Basic
	@Column(name = "ville")
	public String getVille() {
		return ville;
	}

	public void setVille(String ville) {
		this.ville= ville;
	}
	
	@Basic
	@Column(name = "role")
	public char getRole() {
		return role;
	}

	public void setRole(char role) {
		this.role = role;
	}
	
	

	//TODO: pseudo et id ou juste pseudo ?
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserEntity that = (UserEntity) o;
        return id == that.id &&
                Objects.equals(pseudo, that.pseudo);
    }

    public int hashCode() {
        return Objects.hash(id, pseudo);
    }
}
