package comwivme.dto;

public class SeraPresentDto {
	
	protected long iduser;
	protected long idevent;
	
	public long getIduser() {
		return iduser;
	}
	public void setIduser(long iduser) {
		this.iduser = iduser;
	}
	public long getIdevent() {
		return idevent;
	}
	public void setevent(long idevent) {
		this.idevent = idevent;
	}

}
