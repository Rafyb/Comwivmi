package fr.ulille.iut.pizzaland;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.Cache;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.UnsupportedEncodingException;

import static fr.ulille.iut.pizzaland.Connexion.user;
import static fr.ulille.iut.pizzaland.Creation.LOG_TAG;
import static fr.ulille.iut.pizzaland.Creation.VOLLEY_TAG;

public class Affichage_evenement extends AppCompatActivity {

    public final String Fullhostname = "http://10.0.2.2:8080/comwivme";
    private RequestQueue queue;
    Bundle bundle;
    Evenement evenement;
    TextView NOM;
    TextView nature;
    TextView DATE;
    TextView LIEU;
    TextView DESCRIPTION;
    TextView createur;
    Intent intent;
    Evenement evenement_click;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_affichage_evenement);
        bundle = getIntent().getExtras();
        evenement = (Evenement) bundle.getSerializable("evenement");
        NOM = (TextView) findViewById(R.id.NOM);
        NOM.setText(evenement.getTitre());
        nature = (TextView) findViewById(R.id.nature);
        nature.setText(evenement.getNature());
        DATE = (TextView) findViewById(R.id.DATE);
        DATE.setText(evenement.getDate());
        LIEU = (TextView) findViewById(R.id.LIEU);
        LIEU.setText(evenement.getLieu());
        DESCRIPTION = (TextView) findViewById(R.id.DESCRIPTION);
        DESCRIPTION.setText(evenement.getDescription_event());
        createur = (TextView) findViewById(R.id.createur);
        createur.setText(evenement.getCreateur());
        queue = Volley.newRequestQueue(this);

    }

    public void doInscrire(View view){

        Log.d(LOG_TAG, "Send started");
        String base_uri = Fullhostname;
        final String uri = base_uri + "/events/inscription";
        Log.d(LOG_TAG, "Uri: " + uri);

        JSONObject jsonRequest;
        try {
            jsonRequest = new JSONObject("{'idevent':" + evenement.getId() + ", 'iduser':" + user.getId_user() + "}" );

            JsonObjectRequest request = new JsonObjectRequest(
                    Request.Method.POST,
                    uri,
                    jsonRequest,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            showJsonObjectResponse(response);
                            Log.d("JSON",response.toString());

                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Affichage_evenement.this.showError(error);
                        }
                    });
            request.setTag(VOLLEY_TAG);
            queue.add(request);
            Log.d(LOG_TAG, "Send done");
        } catch (Exception e) {
            Log.e(LOG_TAG, "Error while initializing Json request content");
        }
        finish();
    }

    public void showJsonObjectResponse(JSONObject response) {
        try {
            Log.d(LOG_TAG, response.toString(4));
        } catch (JSONException e) {
            Log.d("Erreur JSON", e.getMessage());
        }
    }
    public void showError(VolleyError error) {
        Log.e(LOG_TAG, "Errored Request: " + error.toString());
        if (error.networkResponse == null) {
            //evenement1.setText(getResources().getString(R.string.msg_networkError, error.toString()));
        } else {
            Cache.Entry header = HttpHeaderParser.parseCacheHeaders(error.networkResponse);
            String charset = HttpHeaderParser.parseCharset(header.responseHeaders);
            try {
                String content = new String(error.networkResponse.data, charset);
                //evenement1.setText(getResources().getString(R.string.msg_requestError, content));
            } catch (UnsupportedEncodingException e) {
                Log.e(LOG_TAG, "(response) charset not supported");
                e.printStackTrace();
            }
        }
    }

}
