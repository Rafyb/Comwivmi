package comwivme.dto;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

public class EventDto {
	protected String nom;
	protected LocalDateTime date;
	protected int nbMax;
	protected long idowner;
	protected long id;
	protected String description;
	protected List<UserDto> guests;
	protected String lieu;
	protected String centreinteret;
	protected int prix;
	
	
	public int getPrix() {
		return prix;
	}
	public void setPrix(int prix) {
		this.prix = prix;
	}
	public String getCentreinteret() {
		return centreinteret;
	}
	public void setCentreinteret(String centreinteret) {
		this.centreinteret = centreinteret;
	}
	public String getLieu() {
		return lieu;
	}
	public void setLieu(String lieu) {
		this.lieu = lieu;
	}
	public String getNom() {
		return nom;
	}	
	public void setNom(String nom) {
		this.nom = nom;
	}
	public LocalDateTime getDate() {
		return date;
	}
	public void setDate(LocalDateTime date) {
		this.date = date;
	}

	public int getNbMax() {
		return nbMax;
	}
	public void setNbMax(int nbMax) {
		this.nbMax = nbMax;
	}
	public long getIdOwner() {
		return idowner;
	}
	public void setIdOwner(long idOwner) {
		this.idowner = idOwner;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public List<UserDto> getGuests() {
		return guests;
	}
	public void setGuests(List<UserDto> guests) {
		this.guests = guests;
	}

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EventDto that = (EventDto) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
	
	
}