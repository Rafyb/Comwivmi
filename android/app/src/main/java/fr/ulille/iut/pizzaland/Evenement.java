package fr.ulille.iut.pizzaland;

import java.io.Serializable;

public class Evenement implements Serializable {

    String Titre;
    String Nature;
    String Date;
    String Lieu;
    String Description_event;
    String createur;
    Integer id;
    //String Nbinscrit;

    Evenement(String titre, String nature, String date, String lieu, String description, String createur, Integer id) {
        this.Titre = titre;
        this.Nature = nature;
        this.Date = date;
        this.Lieu = lieu;
        this.Description_event = description;
        this.createur = createur;
        this.id = id;
    }

    public String getTitre(){
        return Titre;
    }

    public String getNature(){
        return Nature;
    }

    public String getDate(){
        return Date;
    }

    public String getLieu(){
        return Lieu;
    }

    public String getDescription_event(){
        return Description_event;
    }

    public String getCreateur(){
        return createur;
    }

    public Integer getId(){ return id;}
}
