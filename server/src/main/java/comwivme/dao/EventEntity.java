package comwivme.dao;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Objects;

import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TypedQuery;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import comwivme.dto.EventDto;
import comwivme.dao.EventEntity;

@Entity
@Cacheable(false)
@Table(name = "event")

@NamedQueries({
	@NamedQuery(name="FindAllEvents", query="SELECT p from EventEntity p"),
	@NamedQuery(name="CheckEventName", query="SELECT count(p) from EventEntity p where p.nom = :pnom and p.id <> :pid"),
	@NamedQuery(name="FindEventByName", query="SELECT p from EventEntity p where p.nom = :pnom"), //TODO: ameliorer ça
	@NamedQuery(name="FindEventById", query="SELECT p from EventEntity p where p.id = :pid"),
	
	@NamedQuery(name="FindEventsByVille", query="SELECT p from EventEntity p where p.lieu = :pville"),
	@NamedQuery(name="FindEventsByInteret", query="SELECT p from EventEntity p where p.centreinteret = :pinteret"),
	@NamedQuery(name="FindEventsByVilleInteret", query="SELECT p from EventEntity p where p.centreinteret = :pinteret and p.lieu = :pville AND p.id <> 1"),//and p.date >= FUNC('CURRENT_TIMESTAMP') ORDER BY p.date ASC"), 
	@NamedQuery(name="FindEventsByIdOwner", query="SELECT p from EventEntity p where p.idOwner = :pidowner") 

}
		)

public class EventEntity extends EventDto {

	private final static Logger logger = LoggerFactory.getLogger(UserEntity.class);
	private static ModelMapper modelMapper = new ModelMapper();

	public EventEntity(EventDto eventDto) {
		modelMapper.map(eventDto, this.getClass());
	}

	public EventEntity() { } 

	public static EventDto convertToDto(EventEntity event) {
		return  modelMapper.map(event, EventDto.class);
	}

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "id", nullable = false)
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	@Basic
	@Column(name = "nom")
	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	@Basic
	@Column(name = "lieu")
	public String getLieu() {
		return lieu;
	}

	public void setLieu(String lieu) {
		this.lieu = lieu;
	}

	@Basic
	@Column(name = "description")
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	
	@Basic
	@Column(name = "centreinteret")
	public String getcentreinteret() {
		return centreinteret;
	}
	public void setcentreinteret(String centreinteret) {
		this.centreinteret = centreinteret;
	}


	@Basic
	@Column(name = "nbMax")
	public int getNbMax() {
		return nbMax;
	}

	public void setNbMax(int nbMax) {
		this.nbMax = nbMax;
	}

	@Basic
	@Column(name = "idowner")
	public long getIdOwner() {
		return idowner;
	}

	public void setIdOwner(long idOwner) {
		this.idowner = idOwner;
	}

	@Basic
	@Column(name = "date")
	public LocalDateTime getDate() {
		return date;
	}

	public void setDate(LocalDateTime date) {
		this.date = date;
	}
	
	@Basic
	@Column(name = "prix")
	public int getPrix() {
		return prix;
	}

	public void setPrix(int prix) {
		this.prix = prix;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		EventEntity that = (EventEntity) o;
		return id == that.id &&
				Objects.equals(nom, that.nom);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, nom);
	}

	@Override
	public String toString() { //TODO: a finir
		return "EventDto [id=" + id + ", nom=" + nom + "]";
	}
}
