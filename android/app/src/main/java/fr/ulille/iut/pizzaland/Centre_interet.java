package fr.ulille.iut.pizzaland;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.GridLayout;
import android.widget.Switch;

import com.android.volley.Cache;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Array;
import java.util.ArrayList;

public class Centre_interet extends AppCompatActivity implements CompoundButton.OnCheckedChangeListener {

    public static final String LOG_TAG = "APPLI";
    public final String Fullhostname = "http://10.0.2.2:8080/comwivme";
    public static final String VOLLEY_TAG = "TEST_PIZZALAND";
    private RequestQueue queue;

    Bundle bundle;
    User user;
    Switch Chant;
    Switch Dessin;
    Switch Photo;
    Switch Anniv;
    Switch Concert;
    Switch Spectacle;
    Switch Jeux_de_role;
    Switch Jeux_video;
    Switch Jeux_de_table;
    Switch Camping;
    Switch Sejours;
    Switch Week_end;
    ArrayList<String> interets = new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_centre_interet);
        bundle = getIntent().getExtras();
        user = (User) bundle.getSerializable("user");
        Chant = (Switch) findViewById(R.id.chant);
        Chant.setOnCheckedChangeListener(this);
        Dessin = (Switch) findViewById(R.id.dessin);
        Dessin.setOnCheckedChangeListener(this);
        Photo = (Switch) findViewById(R.id.photo);
        Photo.setOnCheckedChangeListener(this);
        Anniv = (Switch) findViewById(R.id.anniverssaire);
        Anniv.setOnCheckedChangeListener(this);
        Concert = (Switch) findViewById(R.id.concert);
        Concert.setOnCheckedChangeListener(this);
        Spectacle = (Switch) findViewById(R.id.spectacle);
        Spectacle.setOnCheckedChangeListener(this);
        Jeux_de_role = (Switch) findViewById(R.id.jeux_de_role);
        Jeux_de_role.setOnCheckedChangeListener(this);
        Jeux_video = (Switch) findViewById(R.id.jeux_video);
        Jeux_video.setOnCheckedChangeListener(this);
        Jeux_de_table = (Switch) findViewById(R.id.jeux_de_table);
        Jeux_de_table.setOnCheckedChangeListener(this);
        Camping = (Switch) findViewById(R.id.camping);
        Camping.setOnCheckedChangeListener(this);
        Sejours = (Switch) findViewById(R.id.sejours);
        Sejours.setOnCheckedChangeListener(this);
        Week_end = (Switch) findViewById(R.id.week_end);
        Week_end.setOnCheckedChangeListener(this);

        queue = Volley.newRequestQueue(this);
    }


    public void doInscription2(View view) {


        //hideKeyboard();
        Log.d(LOG_TAG, "Send started");
        String base_uri = Fullhostname;
        final String uri = base_uri + "/users";
        Log.d(LOG_TAG, "Uri: " + uri);

        JSONObject jsonRequest;
        try {
            jsonRequest = new JSONObject("{'pseudo':'" + user.getPseudo() + "','mail':'" + user.getEmail() + "','password':'" + user.getMdp() + "','nom':'" + user.getNom() + "','prenom':'" + user.getPrenom() + "','ville':'" + user.getVille() + "','date_naissance':'" + user.getDate() + "T00:00:00" + "'}");
            JsonObjectRequest request = new JsonObjectRequest(
                    Request.Method.POST,
                    uri,
                    jsonRequest,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            //showJsonObjectResponse(response);
                            Log.d(LOG_TAG, "JSON : " + response.toString());
                            startActivity(new Intent(Centre_interet.this, Page_Accueil.class));

                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Log.d(LOG_TAG, "Error on login : " + error.getMessage());
                            Centre_interet.this.showError(error);
                        }
                    });
            request.setTag(VOLLEY_TAG);
            queue.add(request);
            Log.d(LOG_TAG, "Send done");

            for (String interet : interets) {
                JSONObject jsonRequest2;
                try {
                    jsonRequest2 = new JSONObject("{'pseudo':'" + user.getPseudo() + "','nom':'" + interet + "'}");
                    JsonObjectRequest request2 = new JsonObjectRequest(
                            Request.Method.POST,
                            uri,
                            jsonRequest2,
                            new Response.Listener<JSONObject>() {
                                @Override
                                public void onResponse(JSONObject response) {
                                    //showJsonObjectResponse(response);
                                    Log.d(LOG_TAG, "JSON : " + response.toString());
                                    startActivity(new Intent(Centre_interet.this, Page_Accueil.class));

                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    Log.d(LOG_TAG, "Error on login : " + error.getMessage());
                                    Centre_interet.this.showError(error);
                                }
                            });
                    request.setTag(VOLLEY_TAG);
                    queue.add(request2);
                    Log.d(LOG_TAG, "Send done");
                } catch (Exception e) {
                    Log.e(LOG_TAG, "Error while initializing Json request content");
                }
            }
            finish();
        }catch (Exception e) {
            Log.e(LOG_TAG, "Error while initializing Json request content");
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (buttonView.getId() == R.id.chant) {
            Verifchecked(buttonView, isChecked);
        } else if (buttonView.getId() == R.id.dessin) {
            Verifchecked(buttonView, isChecked);
        } else if (buttonView.getId() == R.id.photo) {
            Verifchecked(buttonView, isChecked);
        } else if (buttonView.getId() == R.id.anniverssaire) {
            Verifchecked(buttonView, isChecked);
        } else if (buttonView.getId() == R.id.concert) {
            Verifchecked(buttonView, isChecked);
        } else if (buttonView.getId() == R.id.spectacle) {
            Verifchecked(buttonView, isChecked);
        }else if(buttonView.getId() == R.id.jeux_de_role){
            Verifchecked(buttonView, isChecked);
        }else if(buttonView.getId() == R.id.jeux_video){
            Verifchecked(buttonView, isChecked);
        }else if(buttonView.getId() == R.id.jeux_de_table){
            Verifchecked(buttonView, isChecked);
        }else if(buttonView.getId() == R.id.camping){
            Verifchecked(buttonView, isChecked);
        }else if(buttonView.getId() == R.id.sejours){
            Verifchecked(buttonView, isChecked);
        }else if(buttonView.getId() == R.id.week_end){
            Verifchecked(buttonView, isChecked);
        }
    }

    private void Verifchecked(CompoundButton buttonView, boolean isChecked) {
        if (isChecked) {
            interets.add(buttonView.getText().toString());
        }
    }

    public void showError(VolleyError error) {
        Log.e(LOG_TAG, "Errored Request: " + error.toString());
        if (error.networkResponse == null) {
            //evenement1.setText(getResources().getString(R.string.msg_networkError, error.toString()));
        } else {
            Cache.Entry header = HttpHeaderParser.parseCacheHeaders(error.networkResponse);
            String charset = HttpHeaderParser.parseCharset(header.responseHeaders);
            try {
                String content = new String(error.networkResponse.data, charset);
                //evenement1.setText(getResources().getString(R.string.msg_requestError, content));
            } catch (UnsupportedEncodingException e) {
                Log.e(LOG_TAG, "(response) charset not supported");
                e.printStackTrace();
            }
        }
    }
}
