#Sprint 25/03  15h30 - 17h

Florentin PLAISANT : abs
Jimmy LETORET : supprimer une activité ( web )
Julien VANRYSSEL : inscription et design ( mobile ) 
Manoel LECOCQ : Integration HTML/CSS sur le site
Nathan FORESTIER : Dernières optimisation du serveur
Raphael BAUVIN : Lister les personnes inscrites sur un événement ( web )

##User story complété :
WEB
-> L'utilisateur peut visionner les personnes inscrites à un évenement
-> L'administrateur peut supprimer des événements
MOBILE
-> L'utilisateur peut s'inscrire 

##Bilan :
Le site possède la majorité des outils métiers les plus importants


##Objectif prochain sprint :
Finaliser le projet et préparer la Démo en vue de la presentation au salon