package comwivme.ressources;

import comwivme.dao.DataAccess;
import comwivme.dao.EventEntity;
import comwivme.dao.SeraPresentEntity;
import comwivme.dao.UserEntity;
import comwivme.dto.EventDto;
import comwivme.dto.UserDto;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriInfo;
import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

@Path("/events")
public class EventRessource {
	final static Logger logger = LoggerFactory.getLogger(EventRessource.class);

	@Context
	public UriInfo uriInfo;

	public EventRessource() {
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<EventDto> getAll() {
		DataAccess dataAccess = DataAccess.begin();
		List<EventEntity> li = dataAccess.getAllEvents();
		dataAccess.closeConnection(true);
		return li.stream().map(EventEntity::convertToDto).collect(Collectors.toList());
	}

	/*
	 * exemple:
	 * curl -H "Content-Type: application/json" \
	 *--request POST \
	 *--data '{"idevent":X,"iduser":Y}' \
	 * http://localhost:8080/comwivme/events/inscription
	 */
	@POST
	@Path("/inscription")
	public Response create(SeraPresentEntity serapresent) {
		DataAccess dataAccess = DataAccess.begin();
		try {
			long id = dataAccess.createSeraPresent(serapresent);
			URI instanceURI = uriInfo.getAbsolutePathBuilder().path("" + id).build();
			dataAccess.closeConnection(true);
			return Response.created(instanceURI).status(201).entity(serapresent).location(instanceURI).build(); //  .created(instanceURI).build();
		}
		catch ( Exception ex ) {
			dataAccess.closeConnection(false);
			System.out.println(ex.getMessage());
			return Response.status(Status.CONFLICT).entity("erreur inscription").build();
		}
	}

	@POST
	public Response create(EventEntity event) {
		try {
		DataAccess dataAccess = DataAccess.begin();
		if (event.getNom() == null) {
			return Response.status(Status.NOT_ACCEPTABLE).entity("name not specified").build();
		}	
		if (event.getDate() == null) {
			return Response.status(Status.NOT_ACCEPTABLE).entity("date not specified").build();
		}
		if (event.getLieu() == null) {
			return Response.status(Status.NOT_ACCEPTABLE).entity("lieu not specified").build();
		}

		if (event.getIdOwner() == 0 ) {
			return Response.status(Status.NOT_ACCEPTABLE).entity("idowner not specified").build();

		}
		//TODO: comment vérifier au niveau du serveur que l'owner est bien l'owner
		try {
			long id = dataAccess.createEvent(event);
			SeraPresentEntity serapresent = new SeraPresentEntity();
			serapresent.setevent(event.getId());
			serapresent.setIduser(event.getIdOwner());
			serapresent.setIdevent(event.getId());
			dataAccess.createSeraPresent(serapresent);
			URI instanceURI = uriInfo.getAbsolutePathBuilder().path("" + id).build();
			dataAccess.closeConnection(true);
			return Response.created(instanceURI).status(201).entity(event).location(instanceURI).build(); //  .created(instanceURI).build();
		}
		catch ( Exception ex ) {
			dataAccess.closeConnection(false);
			return Response.status(Status.CONFLICT).entity("Duplicated name").build();
		}
		}catch(Exception e) {
			System.out.println(e.getMessage());
		}
		return null;
	}
	
	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getId(@PathParam("id") long id) {
		DataAccess dataAccess =DataAccess.begin();
		EventEntity event = dataAccess.getEventById(id);
		if ( event != null ) {
			dataAccess.closeConnection(true);
			return Response.ok(event).build();
		} else {
			dataAccess.closeConnection(false);
			return Response.status(Status.NOT_FOUND).entity("Event not found").build();
		}
	}



	
	/*
	 * exemples:
	 * 	http://localhost:8080/comwivme/events/search&ville=Lille&interet=Chant&prix=50
	 * http://localhost:8080/comwivme/events/search&ville=Paris
	 */
	
	@GET
	@Path("/search{ville:(&ville=[^/]+?)?}{interet:(&interet=[^/]+?)?}{prix:(&prix=[^/]+?)?}")
	@Produces(MediaType.APPLICATION_JSON)
	public List<EventDto> recherche(
			@PathParam("ville") String ville,
			@PathParam("interet") String interet, 
			@PathParam("prix") String prix) {
		
		if(!ville.equals("")) ville = ville.substring(7);
		if(!interet.equals("")) interet = interet.substring(9);
		if(!prix.equals("")) prix = prix.substring(6);
		
		DataAccess dataAccess =DataAccess.begin();
		List<EventEntity> li = dataAccess.getEventByVilleInteretPrix(ville,interet,prix);
		if ( li != null ) {
			dataAccess.closeConnection(true);
		} else {
			dataAccess.closeConnection(false);
			return null;
		}
		return li.stream().map(EventEntity::convertToDto).collect(Collectors.toList());
	}

	

	@GET
	@Path("/ville/{ville}")
	@Produces(MediaType.APPLICATION_JSON)
	public List<EventDto> rechercheVille(@PathParam("ville") String ville) {
		DataAccess dataAccess =DataAccess.begin();
		List<EventEntity> li = dataAccess.getEventByVilleInteret(ville,"");
		if ( li != null ) {
			dataAccess.closeConnection(true);
		} else {
			dataAccess.closeConnection(false);
			return null;
		}
		return li.stream().map(EventEntity::convertToDto).collect(Collectors.toList());
	}


	@GET
	@Path("/interet/{centreinteret}")
	@Produces(MediaType.APPLICATION_JSON)
	public List<EventDto> rechercheInteret(@PathParam("centreinteret") String centreinteret) {
		DataAccess dataAccess =DataAccess.begin();
		List<EventEntity> li = dataAccess.getEventByVilleInteret("",centreinteret);
		if ( li != null ) {
			dataAccess.closeConnection(true);
		} else {
			dataAccess.closeConnection(false);
			return null;
		}
		return li.stream().map(EventEntity::convertToDto).collect(Collectors.toList());
	}

	
	@GET
	@Path("/{id}/inscrits")
	@Produces(MediaType.APPLICATION_JSON)
	public List<UserDto> rechercheInscrits(@PathParam("id") long id) {
		DataAccess dataAccess =DataAccess.begin();
		List<UserEntity> li = dataAccess.getInscritsByEventId(id);
		if ( li != null ) {
			dataAccess.closeConnection(true);
		} else {
			dataAccess.closeConnection(false);
			return null;
		}
		return li.stream().map(UserEntity::convertToDto).collect(Collectors.toList());
	}

	

    @PUT
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response update(@PathParam("id") long id, EventEntity event) {
        DataAccess dataAccess = DataAccess.begin();
        EventEntity eventBDD = dataAccess.getEventById(id);
        if (eventBDD == null) {
            return Response.status(Status.NOT_FOUND).entity("Ingredient not found").build();
        } else {
            try {
                if(event.getNom()!=null) eventBDD.setNom(event.getNom());
                if(event.getcentreinteret()!=null) eventBDD.setcentreinteret(event.getcentreinteret());
                if(event.getCentreinteret()!=null) eventBDD.setCentreinteret(event.getCentreinteret());
                if(event.getDate()!=null) eventBDD.setDate(event.getDate());
                if(event.getDescription()!=null) eventBDD.setDescription(event.getDescription());
                if(event.getLieu()!=null) eventBDD.setLieu(event.getLieu());
                if(event.getNbMax()!=0) eventBDD.setNbMax(event.getNbMax());
                //TODO: test boolean gratuit
                if(event.getPrix()!=0)eventBDD.setPrix(event.getPrix());
                dataAccess.updateEvent(eventBDD);
                dataAccess.closeConnection(true);
                return Response.ok(eventBDD).build(); //  .created(instanceURI).build();
            } catch (Exception ex) {
                dataAccess.closeConnection(false);
                return Response.status(Status.CONFLICT).entity("Duplicated name").build();
            }
        }
    }

    @DELETE
    @Path("/{id}")
    public Response remove(@PathParam("id") long id) {
        DataAccess dataAccess = DataAccess.begin();
		try {
			dataAccess.deleteEvent(id);
			dataAccess.closeConnection(true);
            return Response.status(Status.NO_CONTENT).build();
		} catch (Exception e) {
		    dataAccess.closeConnection(false);
            return Response.status(Status.NOT_FOUND).entity("Event not found").build();
		}
    }
	 
}
