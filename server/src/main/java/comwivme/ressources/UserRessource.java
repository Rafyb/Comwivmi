package comwivme.ressources;

import comwivme.dao.DataAccess;
import comwivme.dao.EventEntity;
import comwivme.dao.InterestEntity;
import comwivme.dao.SeraPresentEntity;
import comwivme.dao.UserEntity;
import comwivme.dto.EventDto;
import comwivme.dto.InterestDto;
import comwivme.dto.SeraPresentDto;
import comwivme.dto.UserDto;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.regex.*;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriInfo;
import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

@Path("/users")
public class UserRessource {
	final static Logger logger = LoggerFactory.getLogger(UserRessource.class);

	@Context
	public UriInfo uriInfo;

	public UserRessource() {
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<UserDto> getAll() {
		DataAccess dataAccess = DataAccess.begin();
		List<UserEntity> li = dataAccess.getAllUsers();
		dataAccess.closeConnection(true);
		return li.stream().map(UserEntity::convertToDto).collect(Collectors.toList());
	}

	@GET
	@Path("/{id}/events")
	@Produces(MediaType.APPLICATION_JSON)
	public List<SeraPresentDto> getIdPresent(@PathParam("id") long id) {
		DataAccess dataAccess =DataAccess.begin();
		List<SeraPresentEntity> presents = dataAccess.getSeraPresentById(id);
		if ( presents != null ) {
			dataAccess.closeConnection(true);
			//return Response.ok(presents).build();
			return presents.stream().map(SeraPresentEntity::convertToDto).collect(Collectors.toList());

		} else {
			dataAccess.closeConnection(false);
			return null;
			//return Response.status(Status.NOT_FOUND).entity("evenements non trouvés").build();
		}
	}




	/*
	 * exemple:
	 * curl -H "Content-Type: application/json" 
	 *--request POST 
	 *--data '{"date":"2001-09-24T00:00:00","nom":"Nord","prenom":"Paul","pseudo":"Polo","email":"polo@gmail.com",
	 * "ville":"Lille","interets":[""],"password":"pwd","role":"U"}' 
	 * http://localhost:8080/comwivme/users
	 */
	@POST
	public Response create(UserEntity user) {
		DataAccess dataAccess = DataAccess.begin();
		if (user.getNom() == null) {
			return Response.status(Status.NOT_ACCEPTABLE).entity("name not specified").build();
		}
		if (user.getPrenom() == null) {
			return Response.status(Status.NOT_ACCEPTABLE).entity("firstname not specified").build();
		}
		if (user.getPseudo() == null) {
			return Response.status(Status.NOT_ACCEPTABLE).entity("pseudo not specified").build();
		}
		if (user.getMail() == null) {
			return Response.status(Status.NOT_ACCEPTABLE).entity("email not specified").build();
		} else {
			Pattern pattern = Pattern.compile(".@.");
			Matcher matcher = pattern.matcher(user.getMail());
			if(!matcher.find()) {
				return Response.status(Status.NOT_ACCEPTABLE).entity("email not acceptable").build();
			}
		}
		if (user.getVille() == null) {
			return Response.status(Status.NOT_ACCEPTABLE).entity("city not specified").build();
		}
		if (user.getPassword() == null) {
			return Response.status(Status.NOT_ACCEPTABLE).entity("password not specified").build();
		}
		if (user.getDate_naissance() == null) {
			return Response.status(Status.NOT_ACCEPTABLE).entity("date not specified").build();
		}

		try {
			long id = dataAccess.createUser(user);
			URI instanceURI = uriInfo.getAbsolutePathBuilder().path("" + id).build();
			dataAccess.closeConnection(true);
			return Response.created(instanceURI).status(201).entity(user).location(instanceURI).build(); //  .created(instanceURI).build();
		}
		catch ( Exception ex ) {
			dataAccess.closeConnection(false);
			return Response.status(Status.CONFLICT).entity("Duplicated name").build();
		}
	}

	@POST
	@Path("/interet")
	public Response create(InterestEntity interet) {
		DataAccess dataAccess = DataAccess.begin();
		if (interet.getNom() == null) {
			return Response.status(Status.NOT_ACCEPTABLE).entity("name not specified").build();
		}
		if (interet.getpseudo() == null) {
			return Response.status(Status.NOT_ACCEPTABLE).entity("pseudo not specified").build();
		}
		try {
			String pseudo = dataAccess.createInteret(interet);
			URI instanceURI = uriInfo.getAbsolutePathBuilder().path("" + pseudo).build();
			dataAccess.closeConnection(true);
			return Response.created(instanceURI).status(201).entity(interet).location(instanceURI).build(); //  .created(instanceURI).build();
		}
		catch ( Exception ex ) {
			dataAccess.closeConnection(false);
			return Response.status(Status.CONFLICT).entity("Duplicated entity").build();
		}
	}


	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getId(@PathParam("id") long id) {
		DataAccess dataAccess =DataAccess.begin();
		UserEntity event = dataAccess.getUserById(id);
		if ( event != null ) {
			dataAccess.closeConnection(true);
			return Response.ok(event).build();
		} else {
			dataAccess.closeConnection(false);
			return Response.status(Status.NOT_FOUND).entity("User not found").build();
		}
	}

	@GET
	@Path("/{id}/ownedevents")
	@Produces(MediaType.APPLICATION_JSON)
	public List<EventDto> getIdOwnerEvents(@PathParam("id") long id) {
		try {
			DataAccess dataAccess = DataAccess.begin();
			List<EventEntity> li = dataAccess.getIdOwnerEvents(id);
			dataAccess.closeConnection(true);
			return li.stream().map(EventEntity::convertToDto).collect(Collectors.toList());
		}catch(Exception e) {
			System.out.println(e.getStackTrace());
			System.out.println(e.getMessage());
		}
		return null;
	}

	@GET
	@Path("/{pseudo}/interets")
	@Produces(MediaType.APPLICATION_JSON)
	public List<InterestDto> getAllInterest(@PathParam("pseudo") String pseudo) {
		try {
			DataAccess dataAccess = DataAccess.begin();
			List<InterestEntity> li = dataAccess.getInterestById(pseudo);
			dataAccess.closeConnection(true);
			return li.stream().map(InterestEntity::convertToDto).collect(Collectors.toList());
		}catch(Exception e) {
			System.out.println(e.getStackTrace());
			System.out.println(e.getMessage());
		}
		return null;
	}

	/*
	 * exemple:
	 * curl -H "Content-Type: application/json" \
	 *--request POST \
	 *--data '{"mail":"aa@gmail.com","password":"pwd"}' \
	 * http://localhost:8080/comwivme/verif
	 */
	@POST
	@Path("/verif")
	public Response verifUser(UserEntity user) {
		DataAccess dataAccess = DataAccess.begin();
		if (user.getMail() == null) {
			return Response.status(Status.NOT_ACCEPTABLE).entity("mail not specified").build();
		}	
		if (user.getPassword() == null) {
			return Response.status(Status.NOT_ACCEPTABLE).entity("pwd not specified").build();
		}

		//TODO: comment vérifier au niveau du serveur que l'owner est bien l'owner
		UserEntity u = dataAccess.verif(user.getMail(), user.getPassword());
		System.out.println("-----------"+user.getMail()+user.getPassword());

		if(u == null) return Response.status(Status.NOT_FOUND).entity("User not found").build();

		String json = u.toString();

		return Response.ok(json,MediaType.APPLICATION_JSON).build();

	}

	/*

    @PUT
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response update(@PathParam("id") long id, IngredientEntity ingredient) {
        DataAccess dataAccess = DataAccess.begin();
        IngredientEntity ingredientBDD = dataAccess.getIngredientById(id);
        if (ingredientBDD == null) {
            return Response.status(Status.NOT_FOUND).entity("Ingredient not found").build();
        } else {
            try {
                ingredientBDD.setNom(ingredient.getNom());
                dataAccess.updateIngredient(ingredientBDD);
                dataAccess.closeConnection(true);
                return Response.ok(ingredientBDD).build(); //  .created(instanceURI).build();
            } catch (Exception ex) {
                dataAccess.closeConnection(false);
                return Response.status(Status.CONFLICT).entity("Duplicated name").build();
            }
        }
    }
	 */
	@DELETE
	@Path("/{id}")
	public Response remove(@PathParam("id") long id) {
		DataAccess dataAccess = DataAccess.begin();
		try {
			dataAccess.deleteUser(id);
			dataAccess.closeConnection(true);
			return Response.status(Status.NO_CONTENT).build();
		} catch (Exception e) {
			dataAccess.closeConnection(false);
			return Response.status(Status.NOT_FOUND).entity("User not found").build();
		}
	}

}
