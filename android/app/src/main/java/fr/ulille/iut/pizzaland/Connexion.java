package fr.ulille.iut.pizzaland;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.android.volley.Cache;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.HttpResponse;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.UnsupportedEncodingException;

public class Connexion extends AppCompatActivity {

    public static final String LOG_TAG = "APPLI";
    public final String Fullhostname = "http://10.0.2.2:8080/comwivme";
    public static final String VOLLEY_TAG = "TEST_PIZZALAND";
    TextView email;
    TextView mdp;
    private RequestQueue queue;
    public static Personne user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_connexion);
        email = (TextView) findViewById(R.id.email_connexion);
        mdp = (TextView) findViewById(R.id.mdp_connexion);
        queue = Volley.newRequestQueue(this);
    }

    public void doInscription(View view){

        startActivity(new Intent(this, Inscription.class));
    }

    public void doConnexion(View view) {

            //hideKeyboard();
            Log.d(LOG_TAG, "Send started");
            String base_uri = Fullhostname;
            final String uri = base_uri + "/users/verif";
            Log.d(LOG_TAG, "Uri: " + uri);

            JSONObject jsonRequest;
            try {
                jsonRequest = new JSONObject("{'mail':'" + email.getText() + "','password':'" + mdp.getText() +"'}" );

                JsonObjectRequest request = new JsonObjectRequest(
                        Request.Method.POST,
                        uri,
                        jsonRequest,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                //showJsonObjectResponse(response);
                                Log.d(LOG_TAG,"JSON : " +response.toString());
                                try {
                                    user = new Personne(response.getInt("id"), response.getString("pseudo"), response.getString("mail"), response.getString("nom"), response.getString("prenom"), response.getString("date_naissance"), response.getString("ville"));
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                startActivity(new Intent(Connexion.this, Page_Accueil.class));

                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Log.d(LOG_TAG, "Error on login : " + error.getMessage());
                                Connexion.this.showError(error);
                            }
                        });
                request.setTag(VOLLEY_TAG);
                queue.add(request);
                Log.d(LOG_TAG, "Send done");
            } catch (Exception e) {
                Log.e(LOG_TAG, "Error while initializing Json request content");
            }

        }

    public void showError(VolleyError error) {
        String responseContent = null;
        if (error.networkResponse == null) {
            responseContent = "Volley Error : " + error.toString();
            //statusCode = STATUS_NETWORK_ERROR;
            Log.d(LOG_TAG, "Passage : je passe là");

        } else {
            int statusCode = error.networkResponse.statusCode;
            if (statusCode == 404) {
                Log.d(LOG_TAG,"404 not found :-)");
            }
        }
    }
    public void showJsonObjectResponse(JSONObject response) {
        try {
            Log.d(LOG_TAG, response.toString(4));
        } catch (JSONException e) {
            Log.d(LOG_TAG,"Erreur JSON" + e.getMessage());
        }
    }

}
