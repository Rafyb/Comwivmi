package fr.ulille.iut.pizzaland;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import fr.ulille.iut.pizzaland.R;

public class Page_Description extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_page_description);
    }

    public void doSuivantRejoindre(View view){

        startActivity(new Intent(this, Page_Description2.class));
    }
}
