package fr.ulille.iut.pizzaland;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.DatePicker;

import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;

public class Calendar2 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.calendar);
    }

    public void doOk(View view) {
        DatePicker dp = (DatePicker) findViewById(R.id.datePicker);
        Intent result = new Intent();
        GregorianCalendar gc = new GregorianCalendar(dp.getYear(), dp.getMonth(), dp.getDayOfMonth());
        result.putExtra("Date", new SimpleDateFormat("yyyy-MM-dd").format(gc.getTime()));
        setResult(RESULT_OK, result);
        finishActivity(Inscription.DATE_REQUEST);
        finish();
    }

    public void doCancel(View view) {

        setResult(RESULT_CANCELED, null);
        finishActivity(Inscription.DATE_REQUEST);
        finish();
    }
}
