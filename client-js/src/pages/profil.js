//@flow
import Component from '../components/Component.js';
import Page from './Page.js';
import ActiviteThumbnail from '../ActiviteThumbnail.js';
import $ from 'jquery';
import PageRenderer from '../PageRenderer.js';
import Popup from '../Popup.js';


export default class Profil extends Page {

    constructor(user){
		super('.content');
		this.user = user;
		// $FlowFixMe
    }
    
    render():string {
        return `
				<div class="container-profil">
								<h1 class="profilName"> Profil de ${this.user.pseudo} ! </h1>
                 <img src="./profile_pictures/0.jpg"/> 
                <div class="profil">
                 <ul>
                 <li>Nom: ${this.user.nom}</li>
                 <li>Prenom: ${this.user.prenom}</li>
                 <li>Date de naissance: ${this.user.date}</li>
                 <li>Ville: ${this.user.ville}</li>
                 <li>Pseudo: ${this.user.pseudo}</li>
                 <li>Email: ${this.user.mail}</li>
             		</ul>
                <form>
                    <button d="modifProfil" type="submit" class="btn btn-default">Modifier profil</button>
							</form>
							</div>

				<form>
				<button style="color:red" id="adminProfil" type="submit" class="btn btn-default">Gérer tout les utilisateurs </button>
				</form>
				<div class="adminPanel">
				</div>

				<form>
				<button id="showHisEvent" type="submit" class="btn btn-default">Afficher mes activités   </button>
				</form>
				<div class="hisevent">
				</div>

				<form>
				<button id="showSubEvent" type="submit" class="btn btn-default">Afficher mes inscriptions</button>
				</form>
				<div class="subevent">
				</div>
				</div>`
		}


		renderProfilAdmin(donnee):void{
			console.log("Dans render Profil Admin");
			console.log(donnee);
			let rendue = `<form>
			<input id="bannirID" type="number" min="2" class="btn btn-default"/>
			<input id="bannirBoutton" type="button" class="btn btn-default">Bannir cette ID</button>
			</form>`;

			donnee.forEach( utilisateur => {
				console.log("Dans render Profil Admin");
				rendue += `
				<ul>
				<li><h2>${this.utilisateur.id}</h2></li>
				<li>Nom: ${this.utilisateur.nom}</li>
				<li>Prenom: ${this.utilisateur.prenom}</li>
				<li>Date de naissance: ${this.utilisateur.date}</li>
				<li>Ville: ${this.utilisateur.ville}</li>
				<li>Pseudo: ${this.utilisateur.pseudo}</li>
				<li>Email: ${this.utilisateur.mail}</li>
				</ul>
				`
			});
			console.log(rendue);
			$('.adminPanel').innerHTML = rendue;

			$(`.bannirBoutton`).click(e =>{
				idUserToBan = document.getElementById('bannirID').value;
				if(PageRenderer.user.role == "A"){
					fetch( `/comwivme/users/${idUserToBan}`, {
							method:'DELETE'
					})
					.then("Utilisateur banni")

			}else{
					alert("Vous n'êtes pas Administrateur")
			}
				
			});
			
		}

		getHisEvent(act){
			act.forEach(a =>{
				this.renderEvents(a,'hisevent');
			});
		}

		afficherProfilAdmin(pro){
			let data:Array=[];
			let idx = 0;
			let profils:Array<{ date:date, id:number, mail:string,	nom:string, password:string, prenom:string,pseudo:string,role:string,ville:string}> = [null];	
			pro.forEach( d =>{
				//console.log(  ((JSON.stringify(d).split(':'))[1].split(','))[0] );
				data[idx] = ((JSON.stringify(d).split(':'))[1].split(','))[0];
				idx++;
			});		
		
				
			console.log(data);
			console.log("Avant render Profil Admin");
			this.renderProfilAdmin(data,'.adminPanel');
							

		}

		getSubEvent(act){
			let data:Array=[];
			let idx = 0;
			act.forEach( d =>{
				//console.log(  ((JSON.stringify(d).split(':'))[1].split(','))[0] );
				data[idx] = ((JSON.stringify(d).split(':'))[1].split(','))[0];
				idx++;
			});		

			let activites:Array<{nom:string, date:date, centreinteret:string, lieu:string, nbMax:number, description:string, id:number, idOwner:number, idParticipant: Array<{id:number}>}> = [null];	
			idx = 0;
			console.log(data);
			data.forEach(d=>{
				console.log(d);
				//console.log(`http://localhost:8080/comwivme/events/${d}`);
				fetch(`http://localhost:8080/comwivme/events/${d}`)
					.then( (response:Response) => {
						return response.json(); 
					})
					.then( (j) => {
						//console.log(JSON.stringify(j));
						activites = j ;
						console.log(activites);
						this.renderEvents(activites,'subevent');
					});
				idx ++;
			});
			//console.log(activites);
		}
			

		renderEvents(activite,div){
			let idx = 0;
			let ids;
			const keyword = "";

			let activ = new ActiviteThumbnail(activite);
			let rendu = activ.render();
			
			/*}else{
				// Avec 1 mot clé pour la recherche
				activites.forEach(element => {
					if(idx>=5*Number(page)&&idx<5*Number(page)+5) {
						if(element.date == keyword || element.description == keyword || element.lieu == keyword || element.nom){
							let activ = new ActiviteThumbnail(element);
							rendu += activ.render();
							rendu += `<div class="popup${idx}">
							<article class="popupContent2">
								<button class="closeButton">X</button>
								<h2>${element.nom}</h2>
								<ul>
									<li>Date : ${element.date}</li>
									<li>Lieu : ${element.lieu}</li>
									<li>Centre d'interet : ${element.centreinteret}</li>
									<li>Decription : ${element.description}</li>
								</ul>
								<button class="postulerButton">Postuler à l'activité !</button>
							</article>
							</div>`;
						}
					}
				idx++;
				}); 
			}*/
			//rendu += `<a href="#" class="pageMoins"><</a> Page ${Number(page)+1} / ${nbPage} <a href="#" class="pagePlus">></a>`;
			if(div == 'hisevent'){
				document.querySelector('.hisevent').innerHTML+=rendu;
			} else {
				document.querySelector('.subevent').innerHTML+=rendu;
			}
			let index = 0;
			activites.forEach(element => {
				if(index>=5*Number(page)&&index<5*Number(page)+5) {
					if(element.date == keyword || element.description == keyword || element.lieu == keyword || element.nom){
						let pop = document.querySelector(`.popup${index}`);
						pop.style.position = "fixed";
						pop.style.top = "100px";
						pop.style.bottom = "100px";
						pop.style.right = "100px";
						pop.style.left = "100px";
						pop.style.display = "none";
						pop.style.background = "white";
					}
				}
				index++;
			});
		
			$(`.media`).click(e =>{
				e.preventDefault();
				const popup:Popup = new Popup(`.${e.delegateTarget.nextSibling.className}`);
				popup.open();
			})
			$(`.pageMoins`).click(e =>{
				e.preventDefault();
				if(page>=1){
					page--;
				}
				renderSubActivite();
			})
			$(`.pagePlus`).click(e =>{
				e.preventDefault();
				if(page+1<nbPage){
					page++;
				}
				renderSubActivite();
			})

   		}
    


}