package fr.ulille.iut.pizzaland;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.Cache;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import static fr.ulille.iut.pizzaland.Filtre.RESULT_VILLE;

public class Recherche extends AppCompatActivity {

    static final int FILTRE_REQUEST = 1;
    private RequestQueue queue;
    public final String Fullhostname = "http://10.0.2.2:8080/comwivme";
    public static final String LOG_TAG = "APPLI";
    public static final String VOLLEY_TAG = "TEST_EVENT";
    ArrayList<Evenement> liste = new ArrayList<Evenement>();
    ArrayList<Evenement> listeevenements = new ArrayList<Evenement>();
    ArrayAdapter<String> arrayadapteur;
    ListView list;
    TextView recherche;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recherche);
        queue = Volley.newRequestQueue(this);
        doGetEvenements();
        recherche = findViewById(R.id.recherche);
        arrayadapteur = new ArrayAdapteur();

        list = findViewById(R.id.liste);
        list.setAdapter(arrayadapteur);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent affichage_evenement = new Intent(fr.ulille.iut.pizzaland.Recherche.this, Affichage_evenement.class);
                Bundle evenement = new Bundle();
                evenement.putSerializable("evenement", listeevenements.get(position));
                affichage_evenement.putExtras(evenement);
                startActivity(affichage_evenement);
            }
        });

    }

    public void showError(VolleyError error) {
        Log.e(LOG_TAG, "Errored Request: " + error.toString());
        if (error.networkResponse == null) {
            //evenement1.setText(getResources().getString(R.string.msg_networkError, error.toString()));
        } else {
            Cache.Entry header = HttpHeaderParser.parseCacheHeaders(error.networkResponse);
            String charset = HttpHeaderParser.parseCharset(header.responseHeaders);
            try {
                String content = new String(error.networkResponse.data, charset);
                //evenement1.setText(getResources().getString(R.string.msg_requestError, content));
            } catch (UnsupportedEncodingException e) {
                Log.e(LOG_TAG, "(response) charset not supported");
                e.printStackTrace();
            }
        }
    }


    public void doGetEvenements() {
        Log.d(LOG_TAG, "Send started");
        String base_uri = Fullhostname;
        final String uri = base_uri + "/events";
        Log.d(LOG_TAG, "Uri: " + uri);

        JsonArrayRequest request = new JsonArrayRequest(
                Request.Method.GET, uri,null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Log.d("Test", "test");
                        showJsonArrayResponse(response);
                        Log.d("requete", response.toString());
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        showError(error);
                    }
                });

        request.setTag(VOLLEY_TAG);
        queue.add(request);
        Log.d(LOG_TAG, "Send done");
    }

    public void showJsonArrayResponse(JSONArray response) {
        try {
            for (int i = 0; i < response.length(); i++) {
                JSONObject obj = response.getJSONObject(i);
                Log.d("Element", obj.getString("nom"));
                listeevenements.add(new Evenement(obj.getString("nom"), obj.getString("centreinteret"), obj.getString("date"), obj.getString("lieu"), obj.getString("description"),obj.getString("idOwner"), obj.getInt("id")));
                Evenement evenement_courant = listeevenements.get(i);
                liste.add(new Evenement(evenement_courant.getTitre(),evenement_courant.getNature(),evenement_courant.getDate(),evenement_courant.getLieu(),evenement_courant.getDescription_event(),evenement_courant.getCreateur(), evenement_courant.getId()));

                Log.d(LOG_TAG, obj.toString());
                arrayadapteur.notifyDataSetChanged();
            }
        } catch (Exception e) {
            Log.d("Erreur", e.getMessage());
        }
    }

    class ArrayAdapteur extends ArrayAdapter {

        public ArrayAdapteur(){
            super(Recherche.this, R.layout.activity_affichage_recherche, liste);
        }


        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            LayoutInflater inflater = getLayoutInflater();
            View row=inflater.inflate(R.layout.activity_affichage_recherche, parent, false);

            TextView centre_interet=(TextView)row.findViewById(R.id.interet_evenement);
            centre_interet.setText(liste.get(position).getNature());

            TextView titre=(TextView)row.findViewById(R.id.titre_evenement);
            titre.setText(liste.get(position).getTitre());

            TextView date =(TextView)row.findViewById(R.id.date);
            date.setText(liste.get(position).getDate());

            TextView description=(TextView)row.findViewById(R.id.description_evenement);
            description.setText(liste.get(position).getDescription_event());

            return(row);
        }
    }

    public void doRecherche(View view){

        String saisierecherche = recherche.getText().toString();
        liste.clear();
        for (Evenement evenement: listeevenements) {

            if(evenement.getNature().equals(saisierecherche) || evenement.getDate().equals(saisierecherche) || evenement.getLieu().equals(saisierecherche) || evenement.getCreateur().equals(saisierecherche)){
                liste.add(evenement);
            }
        }
    }

    public void doFiltre(View view){

        startActivityForResult(new Intent(this, Filtre.class),
                FILTRE_REQUEST);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == FILTRE_REQUEST) {
            if (resultCode == RESULT_OK) {
                String ville = data.getStringExtra("ville");
                String date = data.getStringExtra("date");
                String interet = data.getStringExtra("interet");
                liste.clear();
                for (Evenement evenement : listeevenements) {
                    if(evenement.getLieu().equals(ville) || evenement.getDate().equals(date) || evenement.getNature().equals(interet)){
                        liste.add(evenement);
                    }
                }
                arrayadapteur.notifyDataSetChanged();
            }

        }
    }

}
