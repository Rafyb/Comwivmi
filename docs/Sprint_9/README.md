#Sprint 22/03  10h - 11h30

Florentin PLAISANT : Afficher les details d'un evenement choisi ( web )
Jimmy LETORET : fonctionalité filtrer d'evenement ( web )
Julien VANRYSSEL : Finaliser ajout d'evenement et travail sur connection ( mobile ) 
Manoel LECOCQ : Binome GEA, definition des differentes pages android
Nathan FORESTIER : Integrer les prix aux evenements ( tous )
Raphael BAUVIN : Inscription ( web ),gestion centre d'interet (server)

##User story complété :
WEB
-> L'utilisateur peut ajouter un evenement depuis le mobile
-> L'utilisateur peut filtrer les evenements en faisant une recherche par ville

##Bilan :
Amelioration testé : Meilleur communication
L'utilisateur peut lister et ajouter des evenements depuis mobile et web

##Objectif prochain sprint :
Connection (mobile), afficher un profile (web), garder utilisateur actif (web)
Amelioration : mieux répartir les taches sur les user story
