DROP TABLE IF EXISTS event CASCADE;
--DROP TABLE IF EXISTS ville CASCADE;
DROP TABLE IF EXISTS user CASCADE;
DROP TABLE IF EXISTS serapresent CASCADE;
DROP TABLE IF EXISTS centreinteret CASCADE;
DROP TABLE IF EXISTS centresinteretsusers CASCADE;


CREATE TABLE user (id BIGINT NOT NULL IDENTITY(1,1), pseudo VARCHAR UNIQUE NOT NULL, password VARCHAR NOT NULL, mail VARCHAR UNIQUE NOT NULL, nom VARCHAR, prenom VARCHAR, date_naissance DATE, adresse VARCHAR, ville VARCHAR,role VARCHAR, profilepicture VARCHAR DEFAULT 'static/profile_pictures/0.jpg', PRIMARY KEY (id));

CREATE TABLE event (id BIGINT NOT NULL IDENTITY(1,1), idowner BIGINT NOT NULL, nom VARCHAR NOT NULL, date DATE, nbMax INTEGER, description VARCHAR, lieu VARCHAR, prix INTEGER, centreinteret VARCHAR, PRIMARY KEY (id));

CREATE TABLE serapresent (idevent BIGINT NOT NULL, iduser BIGINT NOT NULL, PRIMARY KEY (idevent, iduser));

CREATE TABLE centreinteret (nom VARCHAR NOT NULL UNIQUE, PRIMARY KEY (nom));

--CREATE TABLE ville(nom VARCHAR NOT NULL UNIQUE, PRIMARY KEY (nom));

CREATE TABLE centresinteretsusers (pseudo VARCHAR NOT NULL, nom VARCHAR NOT NULL, PRIMARY KEY(pseudo,nom));

--ALTER TABLE centresinteretsusers ADD CONSTRAINT FK_centresinteretsusers_pseudo FOREIGN KEY (pseudo) REFERENCES user (pseudo) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE centresinteretsusers ADD CONSTRAINT FK_centresinteretsusers_nom FOREIGN KEY (nom) REFERENCES centreinteret (nom) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE event ADD CONSTRAINT FK_event_centreinteret FOREIGN KEY (centreinteret) REFERENCES centreinteret (nom) ON DELETE CASCADE ON UPDATE CASCADE;

--ALTER TABLE event ADD CONSTRAINT FK_event_lieu FOREIGN KEY (lieu) REFERENCES ville(nom) ON DELETE CASCADE ON UPDATE CASCADE;

--ALTER TABLE user ADD CONSTRAINT FK_user_ville FOREIGN KEY (ville) REFERENCES ville(nom) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE serapresent ADD CONSTRAINT FK_serapresent_idevent FOREIGN KEY (idevent) REFERENCES event (id) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE serapresent ADD CONSTRAINT FK_serapresent_iduser FOREIGN KEY (iduser) REFERENCES user (id) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE event ADD CONSTRAINT FK_event_idowner FOREIGN KEY (idowner) REFERENCES user (id) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE user ADD CONSTRAINT CHK_date_naissance CHECK(DATEDIFF(d, date_naissance, GETDATE()) > 6570); 






insert into user(pseudo,password,mail,nom, prenom,ville,date_naissance, role) values('Admin_Jammy','pwd','webmaster@comwivmi.fr','Admin','1','Lille','1980-01-01','A');
insert into user(pseudo,password,mail,nom, prenom,ville,date_naissance, role) values('Jiji','pwd','aa@gmail.com','Jacque','Jean','Lille','1996-03-17','U');
insert into user(pseudo,password,mail,nom, prenom,ville,date_naissance, role) values('Superman62','pwd','bb@gmail.com','John','Doe','Lens','1999-12-10','U');
insert into user(pseudo,password,mail,nom, prenom,ville,date_naissance, role) values('La Pucelle','pwd','cc@gmail.com','Jeanne','Darc','Orleans','1412-12-10','U');
insert into user(pseudo,password,mail,nom, prenom,ville,date_naissance, role) values('Farel','pwd','felerina.saurel@gmail.com','Falerina','Saurel','Roubaix','1994-5-27','U');
insert into user(pseudo,password,mail,nom, prenom,ville,date_naissance, role) values('Sciusurturs1986','pwd','LandersDeniger@gmail.com','Landers','Deniger','Lens','1986-12-5','U');
insert into user(pseudo,password,mail,nom, prenom,ville,date_naissance, role) values('Vividem','pwd','AgricanChicoine@gmail.com','Agrican','Chicoine','Dunkerque','1971-9-18','U');

insert into centreinteret(nom) values('Chant');
insert into centreinteret(nom) values('Dessin');
insert into centreinteret(nom) values('Photo');
insert into centreinteret(nom) values('Anniversaire');
insert into centreinteret(nom) values('Concert');
insert into centreinteret(nom) values('Spectacle');
insert into centreinteret(nom) values('Jdr');
insert into centreinteret(nom) values('Jeux-video');
insert into centreinteret(nom) values('Table-game');
insert into centreinteret(nom) values('Camping');
insert into centreinteret(nom) values('Sejours');
insert into centreinteret(nom) values('Week-end');

insert into event(idowner,nom,date,nbMax,description,lieu,centreinteret,prix) values('1','Cours de chant','2019-09-24','5','Cours de reggea','Lille','Chant','40');
insert into event(idowner,nom,date,nbMax,description,lieu,centreinteret,prix) values('1','Bal masqué','2019-06-22','10','un super bal masqué sur le théme des animaux','Lens','Spectacle','30');
insert into event(idowner,nom,date,nbMax,description,lieu,centreinteret,prix) values('2','Conférence mathématiques','2019-08-22','15','une super conférence','Roubaix','Spectacle','5');
insert into event(idowner,nom,date,nbMax,description,lieu,centreinteret,prix) values('2','Atelier Dessin','2019-08-23','15','Un atelier où on dessine','Lille','Dessin','5');
insert into event(idowner,nom,date,nbMax,description,lieu,centreinteret,prix) values('4','Exposition photo','2019-07-23','15','Exposition des photos du club','Lille','Photo','2');

insert into serapresent(idevent, iduser) values('1', '2');
insert into serapresent(idevent, iduser) values('1', '3');
insert into serapresent(idevent, iduser) values('1', '4');
insert into serapresent(idevent, iduser) values('2', '4');
insert into serapresent(idevent, iduser) values('2', '5');
insert into serapresent(idevent, iduser) values('2', '1');
insert into serapresent(idevent, iduser) values('3', '4');
insert into serapresent(idevent, iduser) values('3', '5');
insert into serapresent(idevent, iduser) values('3', '1');

insert into centresinteretsusers(pseudo, nom) values('Jiji','Chant');
insert into centresinteretsusers(pseudo, nom) values('Jiji','Dessin');
insert into centresinteretsusers(pseudo, nom) values('Superman62','Anniversaire');
insert into centresinteretsusers(pseudo, nom) values('Superman62','Chant');
insert into centresinteretsusers(pseudo, nom) values('Almeth','Chant');
insert into centresinteretsusers(pseudo, nom) values('Almeth','Camping');

