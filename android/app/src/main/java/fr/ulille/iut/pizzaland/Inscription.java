package fr.ulille.iut.pizzaland;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class Inscription extends AppCompatActivity {

    static final int DATE_REQUEST = 1;
    TextView Nom;
    TextView Prenom;
    TextView Email;
    TextView Mdp;
    TextView Ville;
    TextView Date;
    TextView Pseudo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inscription);
        Nom = (TextView) findViewById(R.id.nom);
        Prenom = (TextView) findViewById(R.id.prenom);
        Email = (TextView) findViewById(R.id.email_inscription);
        Mdp = (TextView) findViewById(R.id.mdp_inscription);
        Ville = (TextView) findViewById(R.id.adresse);
        Date = (TextView) findViewById(R.id.date_naissance);
        Pseudo = (TextView) findViewById(R.id.pseudo);
    }

    public void doCalendar(View view) {

        startActivityForResult(new Intent(this, Calendar2.class),
                DATE_REQUEST);
    }


    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == DATE_REQUEST) {
            if (resultCode == RESULT_OK) {
                TextView tvDate = (TextView) findViewById(R.id.date_naissance);
                String today = data.getStringExtra("Date");
                tvDate.setText(today);
            }

        }
    }

    public void doContinuer(View view) {
        Intent intent = new Intent(this, Centre_interet.class);
        Bundle personne = new Bundle();
        User user = new User(Nom.getText().toString(), Prenom.getText().toString(), Email.getText().toString(), Mdp.getText().toString(), Ville.getText().toString(), Date.getText().toString(),Pseudo.getText().toString());
        personne.putSerializable("user", user);
        intent.putExtras(personne);
        startActivity(intent);
    }


}
