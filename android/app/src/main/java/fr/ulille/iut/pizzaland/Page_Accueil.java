package fr.ulille.iut.pizzaland;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.JsonReader;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.android.volley.Cache;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.UnsupportedEncodingException;

import static fr.ulille.iut.pizzaland.MainActivity.VOLLEY_TAG;

public class Page_Accueil extends AppCompatActivity {

    public final String Fullhostname = "http://10.0.2.2:8080/comwivme";
    public static final String LOG_TAG = "APPLI";
    public static final String VOLLEY_TAG = "TEST_EVENT";
    TextView evenement1;
    TextView evenement2;
    TextView description1;
    TextView description2;
    private RequestQueue queue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_page_accueil);
    }

    public void doRecherche(View view) {
        startActivity(new Intent(this, Recherche.class));
    }

    public void doCreation(View view) { startActivity(new Intent(this, Creation.class));}

    /*public void showError(VolleyError error) {
        Log.e(LOG_TAG, "Errored Request: " + error.toString());
        if (error.networkResponse == null) {
            evenement1.setText(getResources().getString(R.string.msg_networkError, error.toString()));
        } else {
            Cache.Entry header = HttpHeaderParser.parseCacheHeaders(error.networkResponse);
            String charset = HttpHeaderParser.parseCharset(header.responseHeaders);
            try {
                String content = new String(error.networkResponse.data, charset);
                evenement1.setText(getResources().getString(R.string.msg_requestError, content));
            } catch (UnsupportedEncodingException e) {
                Log.e(LOG_TAG, "(response) charset not supported");
                e.printStackTrace();
            }
        }
    }

    public void showJsonArrayResponse(JSONArray response) {
        try {
            for (int i = 0; i < response.length(); i++) {
                JSONObject obj = response.getJSONObject(i);
                Log.d(LOG_TAG, obj.toString());
            }
            JSONObject obj = response.getJSONObject(0);
            JSONObject obj2 = response.getJSONObject(1);
            evenement1.setText(obj.getString("nom"));
            evenement2.setText(obj2.getString("nom"));
            description1.setText(obj.getString("description"));
            description2.setText(obj2.getString("description"));
        } catch (Exception e) {
            evenement1.setText(getResources().getString(R.string.msg_jsonFormatError));
        }
    }

    public void doGetEvenements() {
        Log.d(LOG_TAG, "Send started");
        String base_uri = Fullhostname;
        final String uri = base_uri + "/events";
        Log.d(LOG_TAG, "Uri: " + uri);

        JsonArrayRequest request = new JsonArrayRequest(
                Request.Method.GET, uri, null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        showJsonArrayResponse(response);
                        Log.d("requete", response.toString());
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        showError(error);
                    }
                });
        request.setTag(VOLLEY_TAG);
        queue.add(request);
        Log.d(LOG_TAG, "Send done");
    }*/
}
