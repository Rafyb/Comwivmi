// @flow
import PageRenderer from './PageRenderer.js';
import loginPage from './pages/login.js';
import SignInPages from './signInPage.js';
import $ from 'jquery';
import ActiviteThumbnail from './ActiviteThumbnail.js';
import AddEventPage from './addEventPage.js';
import Popup from './Popup.js';
import Profil from './pages/profil.js';
import HomePage from './pages/HomePage.js';


// configuration du PageRenderer
PageRenderer.titleElement = document.querySelector('section');
PageRenderer.contentElement = document.querySelector('section');

// déclaration des différentes page de l'app
const login = new loginPage();
const inscrire = new SignInPages();
const addEvent = new AddEventPage();
const home = new HomePage();
let liste;
let his = Boolean(false);
let own = Boolean(false);

PageRenderer.renderPage(home);



/*$('.logo').click(e =>{
	e.preventDefault();
	PageRenderer.renderPage(home);
})*/

$(`.eventFormButton`).click(e =>{
	e.preventDefault();
	PageRenderer.renderPage(addEvent);
});

$(`.profilButton`).click(e =>{
	e.preventDefault();
	let his = Boolean(false);
	let own = Boolean(false);
	let profil = new Profil(PageRenderer.user);
	PageRenderer.renderPage(profil);

	$(`#showHisEvent`).click(e =>{
		e.preventDefault();
		if(his){
		$('.hisevent').innerHTML="";
		} else {
		fetch(`http://localhost:8080/comwivme/users/${PageRenderer.user.id}/ownedevents`)
			.then( (response:Response) => response.json() )
			.then( (data) => profil.getHisEvent(data) );
		his = Boolean(true);
		}
	});

	$(`#showSubEvent`).click(e =>{
		e.preventDefault();
		if(own){
			$('.ownevent').innerHTML="";
		} else {
		fetch(`http://localhost:8080/comwivme/users/${PageRenderer.user.id}/events`)
			.then( (response:Response) => response.json() )
			.then( (data) => profil.getSubEvent(data) );
			own = Boolean(true);
		}
	});

});


$(`.profilButton`).hide();
$(`.eventListButton`).hide();
$(`.eventFormButton`).hide();

$(`.loginButton`).click(e=>{
	e.preventDefault();
	PageRenderer.renderPage(login);
	$(`.singinButton`).click(e =>{
		e.preventDefault();
		PageRenderer.renderPage(inscrire);
	});
});


$(`.eventListButton`).click(e =>{
	let rendu:string = `<div class="barreRecherche">
								<h1>Que voulez vous chercher ?</h1>
								<form action="" class="barreRechercheForm">
									<input id="valeurFiltre" name="keyword" class="form-control" type="text" />
									<input id="rechercheButton" class="btn btn-default" type="button" value="Rechercher" />
								<div class="champRecherche">
								<form class="rechercheInteret">
									<select id="valeurInteret" name="interets" class="form-control">
										<option disabled><b>Arts</b></option>
										<option value="Chant">Chant</option>
										<option value="Dessin">Dessin</option>
										<option value="Photo">Photo</option>
				
										<option disabled><b>Evenement</b></option>
										<option value="Anniversaire">Anniversaire</option>
										<option value="Concert">Concert</option>
										<option value="Spectacle">Spectacle</option>
				
										<option disabled><b>Jeux</b></option>
										<option value="Jdr">Jeux de rôle</option>
										<option value="Jeux-video">Jeux Video</option>
										<option value="Table-game">Jeux sur table</option>

										<option disabled><b>Voyages</b></option>
										<option value="Camping">Camping</option>
										<option value="Sejours">Sejours</option>
										<option value="Week-end">Week-End</option>
									</select>
									<input id="buttonInteret" class="btn btn-default" type="button" value="Rechercher" />
								</form>
								<form class="rechercheVille">
									<select id="valeurVille" name="ville" class="form-control">
										<option value="Lille">Lille</option>
										<option value="Tourcoing">Tourcoing</option>
										<option value="Lille">Lille</option>
										<option value="Calais">Calais</option>
										<option value="Bethune">Bethune</option>
										<option value="Roubaix">Roubaix</option>
										<option value="Lens">Lens</option>
										<option value="Dunkerque">Dunkerque</option>
										<option value="Paris">Paris</option>

									</select>
									<input id="buttonVille" class="btn btn-default" type="button" value="Rechercher" />
								</form>
								<form class="recherchePrix">
									<input id="valeurPrix" class="form-control" type="number" min="0" />
									<input id="buttonVille" class="btn btn-default" type="button" value="Rechercher" />
								</form>
								</div>
							</div>`;			

	e.preventDefault();
	let activites:Array<{nom:string, date:date, centreinteret:string, lieu:string, nbMax:number, description:string, id:number, idOwner:number, idParticipant: Array<{id:number}>}> = null;
	let page = 0;
	let nbActivitesPerPage = 5;
	let nbActivites = 0;
	let idx = 0;
	let nbPage = 0;

	function hasKeyWord(element,keyword){
		element.forEach()
	}

	function listeActivite(data):void {
		page = 0;
		nbActivitesPerPage = 5;
		nbActivites = 0;
		idx = 0;
		nbPage = 0;
		activites = data;
		participants(activites);
		activites.forEach(element => {
			nbActivites++;
		});
		nbPage = nbActivites/nbActivitesPerPage;
		if(nbPage>nbPage.toFixed(0)){
			nbPage = Number(nbPage.toFixed(0)) + 1;
		} else {
			nbPage = nbPage.toFixed(0);
		}
		renderActivite();
	}
	

	//Risque de danger
	function mountActivite(container:HTMLElement):void {
		$('form.barreRechercheForm').submit( this.submitRecherche );
	}

	

	function renderActivite(keyword):void {
		console.log(keyword);
		let rendu:string = `	<form class="rechercheInteret">
									<select id="valeurInteret" name="interets" class="form-control">
										<option disabled><b>Arts</b></option>
										<option value="Chant">Chant</option>
										<option value="Dessin">Dessin</option>
										<option value="Photo">Photo</option>
				
										<option disabled><b>Evenement</b></option>
										<option value="Anniversaire">Anniversaire</option>
										<option value="Concert">Concert</option>
										<option value="Spectacle">Spectacle</option>
				
										<option disabled><b>Jeux</b></option>
										<option value="JdR">Jeux de rôle</option>
										<option value="jeux-video">Jeux Video</option>
										<option value="table-game">Jeux sur table</option>

										<option disabled><b>Voyages</b></option>
										<option value="Camping">Camping</option>
										<option value="Sejours">Sejours</option>
										<option value="Week-End">Week-End</option>
									</select>
									<input id="buttonInteret" class="btn btn-default" type="button" value="Recherche par interet" />
									<select id="valeurVille" name="ville" class="form-control">
										<option value="Lille">Lille</option>
										<option value="Roubaix">Roubaix</option>
										<option value="Lens">Lens</option>
										<option value="Dunkerque">Dunkerque</option>
									</select>
									<input id="buttonVille" class="btn btn-default" type="button" value="Rechercher par ville" />
								
									<input id="valeurPrix" class="form-control" type="number" min="0" />
									<input id="buttonPrix" class="btn btn-default" type="button" value="Rechercher par prix" />
									<br>  </br>
									<input id="megaRecherche" class="btn btn-default" type="button" value="MEGA RECHERCHE" />
								</form>
								</div>
							</div>
							<div class="container-listActivity">`
							;			
			
		idx = 0;
		
		if(keyword == ""){
			// Sans mot clé pour la recherche
			activites.forEach(element => {
				if(idx>=5*Number(page)&&idx<5*Number(page)+5) {
					
					let activ = new ActiviteThumbnail(element);
					rendu += activ.render();
				}
				idx++;
			});
		}else{
			// Avec 1 mot clé pour la recherche
			activites.forEach(element => {
				if(idx>=5*Number(page)&&idx<5*Number(page)+5) {
					if(element.date == keyword || element.description == keyword || element.lieu == keyword || element.nom){
						let activ = new ActiviteThumbnail(element);
						rendu += activ.render();	
						rendu += `<div class="popup${idx}">
							<article class="popupContent2">
								<div class="${element.id}">
								</div>
								<button class="closeButton">X</button>
								<h2>${element.nom}</h2>
								<ul>
									<li>Date : ${element.date}</li>
									<li>Lieu : ${element.lieu}</li>
									<li>Centre d'interet : ${element.centreinteret}</li>
									<li>Decription : ${element.description}</li>
									<li class="participants">Participant : ${liste}</li>
								</ul>
								<a href="https://www.prevision-meteo.ch/meteo/localite/${element.lieu.toLocaleLowerCase()}"><img src="https://www.prevision-meteo.ch/uploads/widget/${element.lieu.toLocaleLowerCase()}_0.png" width="650" height="250" /></a>
								<button class="postulerButton">Postuler à l'activité !</button>
								<button class="adminDEL" style="color:red">Supprimer l'activité</button>
							</article>
						</div>`;
					}
				}
				idx++;
			});
			}
			
		rendu += `</div><a href="#" class="pageMoins"><</a> Page ${Number(page)+1} / ${nbPage} <a href="#" class="pagePlus">></a>`;
		document.querySelector('section').innerHTML=rendu;
		$('.adminDEL').hide();
		if(PageRenderer.user.role == "A"){
			$('.adminDEL').show();

		}
		let nbActivitesOnPage = nbActivites;
		if(nbActivitesOnPage>5){
			nbActivitesOnPage=5;
		}
		
		
		let index = 0;
		activites.forEach(element => {
			if(index>=5*Number(page)&&index<5*Number(page)+5) {
				if(element.date == keyword || element.description == keyword || element.lieu == keyword || element.nom){
					let pop = document.querySelector(`.popup${index}`);
					pop.style.position = "fixed";
					pop.style.top = "100px";
					pop.style.bottom = "100px";
					pop.style.right = "100px";
					pop.style.left = "100px";
					pop.style.display = "none";
					pop.style.background = "white";
					pop.style.overflow = "visible";
				}
			}
			index++;

		});
		
		$(`.media`).click(e =>{
			e.preventDefault();
			const popup:Popup = new Popup(`.${e.delegateTarget.nextSibling.className}`);
			popup.open();
		})
		$(`.pageMoins`).click(e =>{
			e.preventDefault();
			if(page>=1){
				page--;
			}
			renderActivite(keyword);
			
		})
		$(`.pagePlus`).click(e =>{
			e.preventDefault();
			if(page+1<nbPage){
				page++;
			}
			renderActivite(keyword);
		})
		$(`#buttonInteret`).click(e=>{
			console.log("On m'as cliqué dessus !")
			e.preventDefault();
			keyword = document.getElementById("valeurInteret").options[document.getElementById('valeurInteret').selectedIndex].value;
			fetch(`http://localhost:8080/comwivme/events/interet/${keyword}`)
			.then( (response:Response) => response.json() )
			.then( listeActivite );
		});

		$(`#buttonVille`).click(e=>{
			console.log("On m'as cliqué dessus !")
			e.preventDefault();
			keyword = document.getElementById("valeurVille").options[document.getElementById('valeurVille').selectedIndex].value;

			fetch(`http://localhost:8080/comwivme/events/ville/${keyword}`)
			.then( (response:Response) => response.json() )
			.then( listeActivite );
		});



		$(`#buttonPrix`).click(e=>{
			console.log("On m'as cliqué dessus !")
			e.preventDefault();
			keyword = document.getElementById('valeurPrix').value;

			fetch(`http://localhost:8080/comwivme/events/search&prix=${keyword}`)
			.then( (response:Response) => response.json() )
			.then( listeActivite );
		});

		$(`#megaRecherche`).click(e=>{
			console.log("On m'as cliqué dessus !")
			e.preventDefault();


			fetch(`http://localhost:8080/comwivme/events/search
			&ville=${document.getElementById("valeurVille").options[document.getElementById('valeurVille').selectedIndex].value}
			&interet=${document.getElementById("valeurInteret").options[document.getElementById('valeurInteret').selectedIndex].value}
			&prix=${document.getElementById('valeurPrix').value}`)
			.then( (response:Response) => response.json() )
			.then( listeActivite );
		});

	}
	fetch('http://localhost:8080/comwivme/events')
		.then( (response:Response) => response.json() )
		.then( listeActivite );
});

/*
$(`.navbar-brand`).click(e =>{
	e.preventDefault();
	PageRenderer.renderPage(homePage);
});*/
	function participants(activites){
	activites.forEach(element => {
	fetch(`http://localhost:8080/comwivme/events/${element.id}/inscrits`)
		.then((response:Response)=> response.json())
		.then((data) =>{
			//onsole.log(JSON.stringify(data));
			let names=[];
			let idx = 0;
			data.forEach(e=>{
				names[idx] = e.pseudo;
				idx++;
			})
			return names;
		})
		.then(names =>{
			liste = '';
			console.log(names);
			names.forEach(n =>{
				liste += ', '
				liste += n;
			});
		console.log(liste);
	});
	});
	}
