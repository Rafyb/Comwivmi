package fr.ulille.iut.pizzaland;

public class Personne {

    Integer id_user;
    String pseudo;
    String mail;
    String nom_user;
    String prenom_user;
    String date_naissance_user;
    String ville;

    Personne(Integer id, String pseudo, String mail, String nom, String prenom, String date_naissance, String ville){

        this.id_user = id;
        this.pseudo = pseudo;
        this.mail = mail;
        this.nom_user = nom;
        this.prenom_user = prenom;
        this.date_naissance_user = date_naissance;
        this.ville = ville;
    }

    public int getId_user(){
        return id_user;
    }

    public String getPseudo(){
        return pseudo;
    }

    public String getMail(){
        return mail;
    }

    public String getNom_user(){
        return nom_user;
    }

    public String getPrenom_user(){
        return prenom_user;
    }

    public String getDate_naissance_user(){
        return date_naissance_user;
    }

    public String getVille(){
        return ville;
    }
}
