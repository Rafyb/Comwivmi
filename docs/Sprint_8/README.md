#Sprint 22/03  8h - 9h30

Florentin PLAISANT : Afficher les details d'un evenement choisi ( web )
Jimmy LETORET : fonctionalité recherche d'evenement ( web )
Julien VANRYSSEL : fonctionnalité ajout d'évenements ( Appli )
Manoel LECOCQ : Binome GEA, definition des differentes pages android
Nathan FORESTIER : Permettre à l'utilisateur de rester connecté et d'avoir acces à ces données profil
Raphael BAUVIN : Finaliser la connexion avec l'utilisateur ( web )

##User story complété :
WEB
-> L'utilisateur peut se connecter avec un mail/mdp existant
-> L'utilisateur peut filtrer les evenements en faisant une recherche par interet

##Bilan :
Amelioration testé : Le temps à mieux été géré, nous avons préparé la Démo 15 min avant et bien complété le README dans les temps
L'utilisateur peut désormais acceder à une page de connection, se connecter avec un identifiant correct et est redirigé sur la page de bienvenue. Depuis cette page il a accès à la liste des evenements qu'il peut filtrer sur le critère de type d'événement.

##Objectif prochain sprint :
Ajout d'evenement ( mobile ), Inscription ( web ), Afficher les details d'activité ( web ), Integrer les prix aux evenements ( tous ), Ajouter des critères de filtre ( web )