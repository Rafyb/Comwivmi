// @flow
import Component from '../components/Component.js';
import PizzaThumbnail from '../components/PizzaThumbnail.js';
import Page from './Page.js';
import $ from 'jquery';

export default class HomePage extends Page {

    constructor(){
		super('.content');
		// $FlowFixMe
    }
    
    render():string {
		return `<body id="page-top">

  <!-- Header -->
  <header class="masthead bg-primary text-white text-center">
    <div class="container">
     <div class="jumbotron">
      <h1 class="text-uppercase mb-0">Bienvenue sur ComWivMi</h1>
      <hr class="star-light">
      <h2 class="font-weight-light mb-0">Site de rencontres autour d'activités et d'évenements</h2>
	</div>
    </div>
  </header>
    
      <!-- About Section -->
  <section class="bg-secondary text-white mb-0" id="about">
    <div class="container">
      <h2 class="text-center text-uppercase text-white">ComWivMi, qu'est-ce que c'est?</h2>
      
      <div class="row">
        <div class="col-lg-4 ml-auto">
          <p class="lead">Morbi tristique enim ultricies lacus imperdiet hendrerit. Mauris efficitur molestie ipsum vel pellentesque. Pellentesque rutrum bibendum aliquam. Quisque ultrices massa ac eleifend blandit. Aliquam eleifend aliquet finibus. Nulla ante ipsum, pellentesque convallis mauris non, bibendum maximus nulla. Nunc molestie interdum suscipit. Phasellus massa eros, scelerisque id accumsan ut, tempor vitae purus. Quisque ac fermentum felis. </p>
        </div>
        <div class="col-lg-4 mr-auto">
          <p class="lead">Morbi tristique enim ultricies lacus imperdiet hendrerit. Mauris efficitur molestie ipsum vel pellentesque. Pellentesque rutrum bibendum aliquam. Quisque ultrices massa ac eleifend blandit. Aliquam eleifend aliquet finibus. Nulla ante ipsum, pellentesque convallis mauris non, bibendum maximus nulla. Nunc molestie interdum suscipit. Phasellus massa eros, scelerisque id accumsan ut, tempor vitae purus. Quisque ac fermentum felis. </p>
        </div>
      </div>
      
    </div>
  </section>

    <section class="portfolio bg-primary" id="portfolio">
    <div class="container">
      <h2 class="text-center text-uppercase text-secondary mb-0">Les activités</h2>
     
      <div class="row">
         <div class="col-md-4">
        <div class="row no-gutters border rounded overflow-hidden flex-md-row mb-4 shadow-sm h-md-250 position-relative">
            <div class="col p-4 d-flex flex-column position-static">
              <strong class="d-inline-block mb-2 text-primary">World</strong>
              <h3 class="mb-0">Featured post</h3>
              <div class="mb-1 text-muted">Nov 12</div>
              <p class="card-text mb-auto">This is a wider card with supporting text below as a natural lead-in to additional content.</p>
              <a href="#" class="stretched-link">Continue reading</a>
            </div>
          
        </div>
      </div>
        <div class="col-md-4">
        <div class="row no-gutters border rounded overflow-hidden flex-md-row mb-4 shadow-sm h-md-250 position-relative">
        <div class="col p-4 d-flex flex-column position-static">
          <strong class="d-inline-block mb-2 text-primary">World</strong>
          <h3 class="mb-0">Featured post</h3>
          <div class="mb-1 text-muted">Nov 12</div>
          <p class="card-text mb-auto">This is a wider card with supporting text below as a natural lead-in to additional content.</p>
          <a href="#" class="stretched-link">Continue reading</a>
        </div>
       
            </div>
		</div>

 <div class="col-md-4">
        <div class="row no-gutters border rounded overflow-hidden flex-md-row mb-4 shadow-sm h-md-250 position-relative">
        <div class="col p-4 d-flex flex-column position-static">
          <strong class="d-inline-block mb-2 text-primary">World</strong>
          <h3 class="mb-0">Featured post</h3>
          <div class="mb-1 text-muted">Nov 12</div>
          <p class="card-text mb-auto">This is a wider card with supporting text below as a natural lead-in to additional content.</p>
          <a href="#" class="stretched-link">Continue reading</a>
        </div>

            </div>	
      </div>
      
      
    </div>
      </div>
    </div>
  </section>
    
    
          <!-- About Section -->
  <section class="bg-secondary text-white mb-0 getTheApp" id="activity">
	
    <div class="container">
		<div class="jumbotron">
      <h2 class="text-center text-uppercase text-white">Convaincu? Ca sera mieux sur l'app</h2>
      <hr class="star-light mb-5">
      <div class="row">
        <div class="col-lg-12 ml-auto">
          <p class="lead">Morbi tristique enim ultricies lacus imperdiet hendrerit. Mauris efficitur molestie ipsum vel pellentesque. Pellentesque rutrum bibendum aliquam. Quisque ultrices massa ac eleifend blandit. Aliquam eleifend aliquet finibus. Nulla ante ipsum, pellentesque convallis mauris non, bibendum maximus nulla. Nunc molestie interdum suscipit. Phasellus massa eros, scelerisque id accumsan ut, tempor vitae purus. Quisque ac fermentum felis. </p>
        </div>
      </div>    
         <div class="form-group text-center ">
              <button type="submit" class="btn btn-primary btn-xl" id="sendMessageButton">Get the app</button>
            </div>
</div>
    </div>
  </section>`
	}


}
