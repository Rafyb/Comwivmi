package fr.ulille.iut.pizzaland;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import static fr.ulille.iut.pizzaland.Inscription.DATE_REQUEST;

public class Filtre extends AppCompatActivity implements AdapterView.OnItemSelectedListener{

    static final int RESULT_VILLE = 1;
    TextView ville;
    TextView filtre_date;
    Spinner filtre_interet;
    String saisieinteret;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filtre);
        ville = findViewById(R.id.ville);
        filtre_date = findViewById(R.id.filtre_date);
        filtre_interet = findViewById(R.id.spinner_interet);
        ArrayAdapter centre_interet = ArrayAdapter.createFromResource(this, R.array.Centre_interet, android.R.layout.simple_spinner_item);
        filtre_interet.setAdapter(centre_interet) ;
        filtre_interet.setOnItemSelectedListener(this);

    }

    public void doCalendar2(View view) {

        startActivityForResult(new Intent(this, Calendar2.class),
                DATE_REQUEST);
    }


    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == DATE_REQUEST) {
            if (resultCode == RESULT_OK) {
                TextView tvDate = (TextView) findViewById(R.id.filtre_date);
                String today = data.getStringExtra("Date");
                tvDate.setText(today);
            }

        }
    }

    public void doValider(View view){

        String filtre_ville = ville.getText().toString();
        String date = filtre_date.getText().toString();
        Intent result = new Intent();
        result.putExtra("ville", filtre_ville);
        result.putExtra("date", date);
        if(saisieinteret != null){
            result.putExtra("interet", saisieinteret);
        }
        setResult(RESULT_OK, result);
        finishActivity(Recherche.FILTRE_REQUEST);
        finish();
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        if(parent.getId() == R.id.spinner_interet) {
            saisieinteret = parent.getSelectedItem().toString();
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
    }
}
