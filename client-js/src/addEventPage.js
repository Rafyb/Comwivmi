// @flow
import Page from './pages/Page.js';
import $ from 'jquery';
import PageRenderer from './PageRenderer.js';

export default class AddEventPage extends Page {
	constructor(){
		super('.content');
		// $FlowFixMe
		this.submit = this.submit.bind(this);
	}

	render():string {
		return `
<div class="container-addEvent"><form class="addEventForm">
		<label>
			Nature :
			<input type="text" name="nature" class="form-control">
		</label>
	
		<label>
			Date de naissance :
			<input type="date" name="date" class="form-control" >
		</label>

		<label>
			Nombre Maximum de participant :
			<input type="number" name="nbMax" min='1' max='15' class="form-control" >
		</label>
		
		<label>
			Lieu :
			<select name="ville"  class="form-control">
				<option>Lille</option>
				<option>Dunkerque</option>
				<option>Lens</option>
				<option>Roubaix</option>
			</select>
		</label>
		<label>
			Centre d'interets :
			<select name="interets" multiple="true" class="form-control">
			<option disabled><b>Arts</b></option>
			<option value="Chant">Chant</option>
			<option value="Dessin">Dessin</option>
			<option value="Photo">Photo</option>

			<option disabled><b>Evenement</b></option>
			<option value="Anniversaire">Anniversaire</option>
			<option value="Concert">Concert</option>
			<option value="Spectacle">Spectacle</option>

			<option disabled><b>Jeux</b></option>
			<option value="Jdr">Jeux de rôle</option>
			<option value="Jeux-video">Jeux Video</option>
			<option value="Table-game">Jeux sur table</option>

			<option disabled><b>Voyages</b></option>
			<option value="Camping">Camping</option>
			<option value="Sejours">Sejours</option>
			<option value="Week-end">Week-End</option>
			</select>
		</label>
		<label>
			Description :
			<input type="text" name="description" class="form-control">
		</label>
		<label>
			Prix :
			<input type="number" name="prix" min="0" class="form-control">
		</label>
		<button type="submit" class="btn btn-default">Ajouter</button>
	</form>
</div>`;
	}

	mount(container:HTMLElement):void {
		$('form.addEventForm').submit( this.submit );
	}

	submit(event:Event):void {
		event.preventDefault();
		const fieldNames:Array<string> = [
			'interets',
			'date',
			'description',
			'ville',
			'nbMax',
			'nature',
			'prix'
		];
		// on récupère la valeur saisie dans chaque champ
		const values:any = {};
		const errors:Array<string> = [];

		fieldNames.forEach( (fieldName:string) => {
			const value = this.getFieldValue(fieldName);
			if ( !value ){
				errors.push( `Le champ ${fieldName} ne peut être vide !` );
			}
			values[fieldName] = value;
		});
		if (errors.length) {
			// si des erreurs sont détectées, on les affiche
			alert( errors.join('\n') );
		} else {
			// si il n'y a pas d'erreur on envoie les données
			const evenement = {
				centreinteret:values.interets[0],
				date:values.date+"T00:00:00",
				description:values.description,
				idOwner:PageRenderer.user.id,
				lieu:values.ville[0],
				nbMax:parseInt(values.nbMax,10),
				nom:values.nature,
				prix:values.prix
			};
			console.log(evenement);
			console.log(JSON.stringify(evenement));
			fetch( '/comwivme/events', {
					method:'POST',
					headers: { 'Content-Type': 'application/json' },
					body: JSON.stringify(evenement)
				})
			.then(response => {
				if (!response.ok) {
					throw new Error( `${response.status} : ${response.statusText}` );
				}
				return response.json();
			})
			.then ( event => {
				alert(`Evenement "${event.nom}" enregistrée avec succès ! `);
				// puis on vide le formulaire
				const form:?HTMLElement = document.querySelector('form.addEventForm');
				if (form && form instanceof HTMLFormElement) {
					form.reset();
				}
			})
			.catch( error => alert(`Enregistrement impossible : ${error.message}`) );
		}
	}

	getFieldValue(fieldName:string):?string|Array<string>{
		// on récupère une référence vers le champ qui a comme attribut `name` la valeur fieldName (nom, base, prix_petite, etc.)
		const field:?HTMLElement = document.querySelector(`[name=${fieldName}]`);
		if ( field instanceof HTMLInputElement ) {
			// s'il s'agit d'un <input> on utilise la propriété `value`
			// et on retourne la chaine de caractère saisie
			return field.value != '' ? field.value : null;
		} else if ( field instanceof HTMLSelectElement ) {
			// s'il s'agit d'un <select> on utilise la propriété `selectedOptions`
			const values:Array<string> = [];
			for (let i = 0; i < field.selectedOptions.length; i++) {
				values.push( field.selectedOptions[i].value );
			}
			// et on retourne un tableau avec les valeurs sélectionnées
			return values.length ? values : null;
		}
		return null;
	}
}