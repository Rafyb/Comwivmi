package comwivme;

import org.glassfish.jersey.server.ResourceConfig;

import javax.ws.rs.ApplicationPath;

@ApplicationPath("/comwivme/")
public class ApiV1 extends ResourceConfig {

    public ApiV1() {
        packages("comwivme.ressources");
        register(CORSFilter.class);

    }

}

