// @flow
import $ from 'jquery';
import PageRenderer from './PageRenderer.js';
export default class Popup {
	tagName:string;

	constructor( tagName:string){
        this.tagName = tagName;
    }
    
    //fonction pour ouvrir la popup
	open():void{
        const pop:?HTMLElement = document.querySelector(this.tagName);
        if (pop) {
            pop.style.display = "inline";
        }
        
    
        $(`.closeButton`).click(e =>{
			e.preventDefault();
			this.close();
        })
        $(`.postulerButton`).click(e =>{
            e.preventDefault();
            console.log(`je passe`);
			this.postuler(e.delegateTarget.parentElement.firstElementChild.className);
        })

        $(`.adminDEL`).click(e =>{
            e.preventDefault();
            console.log(`je supprime`);
			this.supprimer(e.delegateTarget.parentElement.firstElementChild.className);
        })
        
    }

    supprimer(idEvent):void{
        if(PageRenderer.user.role == "A"){
            fetch( `/comwivme/events/${idEvent}`, {
                method:'DELETE'
            })
            .then("Annonce supprimée")

        }else{
            alert("Vous n'êtes pas Administrateur")
        }
    }

    postuler(idEvent):void {
        const pop:?HTMLElement = document.querySelector(this.tagName);
        console.log(`Je passe dans ${this.tagName}`);
        pop.innerHTML = `<div class="${this.tagName}">
                            <article class="popupContent2">
                                <button class="closeButton">X</button>
                                <h2>Demande enregistré</h2>
                            </article>
                        </div>`;
        const participation = {
            idevent: parseInt(idEvent),
            iduser: parseInt(PageRenderer.user.id)
        };
        fetch( '/comwivme/events/inscription', {
                method:'POST',
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify(participation)
            })
        .then(response => {
            if (!response.ok) {
                throw new Error( `${response.status} : ${response.statusText}` );
            }
            // ici on recupere les infos
            return response.json();
        })
        .then ( reponse => {
        })
        .catch( error => alert(`Connection impossible : ${error.message}`) );

        $(`.closeButton`).click(e =>{
            e.preventDefault();
            this.close();
        })
    }
    
    //fonction pour fermer la popup lors du clique sur la croix de la popup
	close():void {
        const pop:?HTMLElement = document.querySelector(this.tagName);
        if (pop) {
            pop.style.display = "none";
        }
        $(`.postulerButton`).off()
	}
}