package comwivme.dto;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;
import java.util.Objects;

public class UserDto {
	protected long id;
	protected String nom;
	protected String prenom;
	protected String pseudo;
	//TODO: private image de profile
	protected LocalDateTime date_naissance;
	protected List<String> centresDinterets;
	protected String ville;
	protected String password;
	protected String mail;
	protected char role;
	
	public char getRole() {
		return role;
	}

	public void setRole(char role) {
		this.role = role;
	}

	public List<String> getCentresDinterets() {
		return centresDinterets;
	}

	public void setCentresDinterets(List<String> centresDinterets) {
		this.centresDinterets = centresDinterets;
	}

	public String getVille() {
		return ville;
	}

	public void setVille(String ville) {
		this.ville = ville;
	}

    public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getPseudo() {
		return pseudo;
	}

	public void setPseudo(String pseudo) {
		this.pseudo = pseudo;
	}

	public LocalDateTime getDate_naissance() {
		return date_naissance;
	}

	public void setDate_naissance(LocalDateTime date_naissance) {
		this.date_naissance = date_naissance;
	}

	@Override
    public int hashCode() {
        return Objects.hash(id);
    }
	
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserDto that = (UserDto) o;
        return Objects.equals(id, that.id);
    }
	
}