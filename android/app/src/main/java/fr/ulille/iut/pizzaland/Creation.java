package fr.ulille.iut.pizzaland;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.Cache;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.UnsupportedEncodingException;

import static fr.ulille.iut.pizzaland.Connexion.user;

public class Creation extends AppCompatActivity implements AdapterView.OnItemSelectedListener{

    public final String Fullhostname = "http://10.0.2.2:8080/comwivme";
    static final int DATE_REQUEST = 1;
    public static final String LOG_TAG = "APPLI";
    public static final String VOLLEY_TAG = "TEST_COMWIVMI";
    TextView Titre;
    TextView Date;
    Spinner Nbparticipants;
    TextView Lieu;
    Spinner Interet;
    TextView Description_activite;
    String centreinteret;
    Integer nombreparticipants;
    private RequestQueue queue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_creation);
        Titre = (TextView) findViewById(R.id.titre_activite);
        Date = (TextView) findViewById(R.id.date_activite);
        Nbparticipants = findViewById(R.id.nbparticipants);
        ArrayAdapter participants = ArrayAdapter.createFromResource(this, R.array.NbParticipants, android.R.layout.simple_spinner_item);
        Nbparticipants.setAdapter(participants) ;
        Nbparticipants.setOnItemSelectedListener(this);
        Lieu = (TextView) findViewById(R.id.lieu);
        Interet = findViewById(R.id.interet);
        ArrayAdapter centre_interet = ArrayAdapter.createFromResource(this, R.array.Centre_interet, android.R.layout.simple_spinner_item);
        Interet.setAdapter(centre_interet) ;
        Interet.setOnItemSelectedListener(this);
        Description_activite = (TextView) findViewById(R.id.description_activite);
        queue = Volley.newRequestQueue(this);

    }

    public void doCalendar(View view) {

        startActivityForResult(new Intent(this, Calendar2.class),
                DATE_REQUEST);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == DATE_REQUEST) {
            if (resultCode == RESULT_OK) {
                TextView tvDate = (TextView) findViewById(R.id.date_activite);
                String today = data.getStringExtra("Date");
                tvDate.setText(today);
            }

        }
    }

    public void doCreer(View view){
            //hideKeyboard();
            Log.d(LOG_TAG, "Send started");
            String base_uri = Fullhostname;
            final String uri = base_uri + "/events";
            Log.d(LOG_TAG, "Uri: " + uri);

            JSONObject jsonRequest;
            try {
                jsonRequest = new JSONObject("{'centreinteret':'" +  centreinteret + "','date':'"
                        +  Date.getText() + "T00:00:00'" + ",'description':'" + Description_activite.getText()
                        + "','idOwner':" + user.getId_user() + ",'lieu':'" + Lieu.getText() + "','nbMax':'" + nombreparticipants
                        + "','nom':'" + Titre.getText() +"'}" );

                JsonObjectRequest request = new JsonObjectRequest(
                        Request.Method.POST,
                        uri,
                        jsonRequest,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                showJsonObjectResponse(response);
                                Log.d("JSON",response.toString());

                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Creation.this.showError(error);
                            }
                        });
                request.setTag(VOLLEY_TAG);
                queue.add(request);
                Log.d(LOG_TAG, "Send done");
            } catch (Exception e) {
                Log.e(LOG_TAG, "Error while initializing Json request content");
            }
            finish();
        }

    public void showJsonObjectResponse(JSONObject response) {
        try {
            Log.d(LOG_TAG, response.toString(4));
        } catch (JSONException e) {
            Log.d("Erreur JSON", e.getMessage());
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if(parent.getId() == R.id.interet) {
            centreinteret = parent.getSelectedItem().toString();
        }else {
            nombreparticipants =  Integer.parseInt(parent.getSelectedItem().toString());
        }
    }


    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    public void showError(VolleyError error) {
        Log.e(LOG_TAG, "Errored Request: " + error.toString());
        if (error.networkResponse == null) {
            //evenement1.setText(getResources().getString(R.string.msg_networkError, error.toString()));
        } else {
            Cache.Entry header = HttpHeaderParser.parseCacheHeaders(error.networkResponse);
            String charset = HttpHeaderParser.parseCharset(header.responseHeaders);
            try {
                String content = new String(error.networkResponse.data, charset);
                //evenement1.setText(getResources().getString(R.string.msg_requestError, content));
            } catch (UnsupportedEncodingException e) {
                Log.e(LOG_TAG, "(response) charset not supported");
                e.printStackTrace();
            }
        }
    }

}
