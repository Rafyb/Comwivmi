// @flow
import Component from './components/Component.js';
import Img from './components/Img.js';


export default class ActiviteThumbnail extends Component {
	constructor(activite:{nom:string, date:date, lieu:string, nbMax:number, description:string, id:number, idOwner:number, idParticipant: Array<{id:number}>}){
		const link:string = `#`;
		super('article', {name:'class', value:'media'}, [
			new Component( 'a', {name:'href', value: link}, [
				new Component('section', {name:'class', value: 'infos'}, [
					new Component('h4', null, activite.nom),
					new Component( 'ul', null, [
						new Component('li', null, `Date : ${activite.date} `),
                        new Component('li', null, `Lieu : ${activite.lieu} `),
                        new Component('li', null, `Nombre maximum de participant : ${activite.nbMax} `),
                        new Component('li', null, `Description : ${activite.description} `),
					])
				])
			])
		]);
	}
}