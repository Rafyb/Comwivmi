package comwivme.dao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.qos.logback.core.net.SyslogOutputStream;
import comwivme.dao.EventEntity;
import comwivme.dao.DatabaseConstraintException;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.*;

public class DataAccess {
	private final static Logger logger = LoggerFactory.getLogger(DataAccess.class);
	private EntityManager em;
	private EntityTransaction et;

	// global operations

	/**
	 * Procedure d'initialisation de la connexion.
	 * Crée un objet dataAccess et ouvre la transction.
	 * La connexion doit être fermée par un appel à {@link #closeConnection(boolean)}.
	 * @return L'objet {@link DataAccess} permettant l'accès à la base.
	 */
	public synchronized static DataAccess begin() {
		return new DataAccess();
	}

	/**
	 * Termine la connexion sur laqualle cette méthode est appliquée (avec ou sans validation).
	 * Si commit vaut true, les opérations sont effectivement écrites dans la BDD,
	 * sinon, les opérations sont ignorées.
	 * @param commit ignore les opérations effectuées.
	 */
	public void closeConnection(boolean commit) {
		if (commit) {
			this.commit();
		} else {
			this.rollback();
		}
		em.close();
	}

	/**
	 * Valide toutes les opérations BDD de la connexion courante.
	 */
	private void commit() {
		et.commit();
	}

	/**
	 * Annule toutes les opérations BDD de la connexion courante.
	 */
	private void rollback() {
		et.rollback();
	}

	/**
	 * Crée un objet connection et initialise une transaction BDD.
	 */
	private DataAccess() {
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("comwivmePersistenceUnit");
		em = emf.createEntityManager();
		et = em.getTransaction();
		et.begin();
	}

	// database operations

	/**
	 * Charge la liste de tous les utilisateurs de la base
	 * @return la liste de tous les utilisateurs
	 */
	public List<UserEntity> getAllUsers() {
		TypedQuery<UserEntity> query = em.createNamedQuery("FindAllUsers", UserEntity.class);
		return query.getResultList();
	}

	public List<InterestEntity> getAllInterest() {
		TypedQuery<InterestEntity> query = em.createNamedQuery("FindAllInterest", InterestEntity.class);
		return query.getResultList();
	}

	/**
	 * Recherche d'un utilisateur à partir de son id.
	 * retourne null si aucun utilisateur de la base ne possède cet id.
	 * @param idUser l'id recherché
	 * @return L'utilisateur si il existe
	 */
	public UserEntity getUserById(long idUser) {
		return em.find(UserEntity.class, idUser);
	}



	// Event operations

	/**
	 * Lecture de la totalités des events de la base
	 * @return La liste des events
	 */
	public List<EventEntity> getAllEvents() {
		TypedQuery<EventEntity> query = em.createNamedQuery("FindAllEvents", EventEntity.class);
		return query.getResultList();
	}


	public List<SeraPresentEntity> getSeraPresentByEventId(long id) {
		List<SeraPresentEntity> returnValue = null;
		try {
			TypedQuery<SeraPresentEntity> query = null;
			query = em.createNamedQuery("FindSeraPresentByIdEvent", SeraPresentEntity.class);
			query.setParameter("pid", id);
			returnValue = query.getResultList();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return returnValue;
	}

	public List<UserEntity> getInscritsByEventId(long id) {
		List<SeraPresentEntity> ids = getSeraPresentByEventId(id);
		List<UserEntity> inscrits = new LinkedList<UserEntity>();
		System.out.println(ids.size());
		try {
			for(SeraPresentEntity spe : ids) {
				TypedQuery<UserEntity> query = null;
				query = em.createNamedQuery("FindUserById", UserEntity.class);
				query.setParameter("pid", spe.getIduser());
				inscrits.add(query.getSingleResult());
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return inscrits;
	}

	/**
	 * Lecture de 
	 * @return La liste des events
	 */

	@SuppressWarnings("unchecked")
	public List<EventEntity> getEventByVilleInteretPrix(String ville, String interet, String prix) {
		String query = "SELECT p from EventEntity p";
		boolean premierparam = true;


		if(!ville.equals("")) {
			query+=" where p.lieu = :pville";
			premierparam=false;
		}
		if(!interet.equals("")) {
			if(!premierparam) {
				query+=" AND p.centreinteret = :pinteret";
			}
			else {
				query+=" where p.centreinteret = :pinteret";
				premierparam=false;
			}
		}
		if(!prix.equals("")) {
			System.out.println(prix);
			if(!premierparam) {
				query+=" AND p.prix <= :pprix";
			}
			else {
				query+=" where p.prix <= :pprix";
				premierparam=false;
			}
		}

		//java.sql.Date dateString = Date.valueOf(LocalDate.now()); 
		LocalDateTime dateString = LocalDateTime.now();
		if(!premierparam) {
			query+=" and p.date >= '"+dateString.toString()+"' ORDER BY p.date ASC";
		}
		else {
			query+=" where p.date >= '"+dateString.toString()+"' ORDER BY p.date ASC";
			premierparam=false;
		}
		System.out.println("AZERTY\n"+query);
		try {
			TypedQuery<EventEntity> qquery = em.createQuery(query,EventEntity.class);
			if(!prix.equals("")) qquery.setParameter("pprix", Integer.parseInt(prix));
			if(!ville.equals("")) qquery.setParameter("pville", ville);
			if(!interet.equals("")) qquery.setParameter("pinteret", interet);
			return qquery.getResultList();
		}catch(Exception e) {
			System.out.println(e.getMessage());
			System.out.println(e.getStackTrace());
		}
		return null;
	}

	public List<EventEntity> getEventByVilleInteret(String ville, String interet) {
		TypedQuery<EventEntity> query = null;
		if(ville.equals("")) {
			query = em.createNamedQuery("FindEventsByInteret", EventEntity.class);
			query.setParameter("pinteret", interet);
		}
		else if(interet.equals("")) {
			query = em.createNamedQuery("FindEventsByVille", EventEntity.class);
			query.setParameter("pville", ville);
		}else {
			query = em.createNamedQuery("FindEventsByVilleInteret", EventEntity.class);
			query.setParameter("pinteret", interet);
			query.setParameter("pville", ville);
		}
		return query.getResultList();
	}


	/**
	 * Crée un évenement
	 * Doit préciser le lieu, le nom, la date
	 * Ne peut pas avoir le même nom qu'un évenement précedemment enregistré
	 * L'id (généré par la BDD) est renseigné automatiquement après l'ajout.
	 * @param l'event à ajouter
	 * @return L'id de l'event ajouté
	 * @throws DatabaseConstraintException si un evenement au même nom existe déjà
	 */
	public long createEvent(EventEntity event) throws DatabaseConstraintException {
		try {
			em.persist(event);
			em.flush();
		} catch (PersistenceException e) {
			//TODO: je sais pas si l'exception est jamais lancée
			throw new DatabaseConstraintException();
		}
		return event.getId();
	}

	/**
	 * Recherche d'un event à partir de son id.
	 * retourne null si aucun event de la base ne possède cet id.
	 * retourne null si il existe plusieurs event de cet id.
	 * @param id l'id recherché
	 * @return L'event si il existe
	 */
	public EventEntity getEventById(long id) {
		EventEntity returnValue;
		TypedQuery<EventEntity> query = em.createNamedQuery("FindEventById", EventEntity.class);
		query.setParameter("pid", id);
		try {
			returnValue = query.getSingleResult();
		} catch (NonUniqueResultException | NoResultException e) {
			returnValue = null;
		}
		return returnValue;
	}

	public UserEntity verif(String mail, String pwd) {
		UserEntity returnValue;
		TypedQuery<UserEntity> query = em.createNamedQuery("VerifUser", UserEntity.class);
		System.out.println("111111"+mail+" "+pwd);
		query.setParameter("mail", mail);
		query.setParameter("password", pwd);
		try {
			returnValue = query.getSingleResult();
		} catch (NonUniqueResultException | NoResultException e) {
			returnValue = null;
		}
		return returnValue;
	}

	public void deleteEvent(long id) throws Exception {
		EventEntity event = em.find(EventEntity.class,  id);
		if (event == null) throw new Exception();
		em.remove(em.merge(event));
	}

	public void updateEvent(EventEntity event) throws DatabaseConstraintException {
		try {
			em.flush();
		} catch (PersistenceException e) {
			throw new DatabaseConstraintException();
		}
	}

	//user operations
	/**
	 * Crée un utilisateur
	 * Doit préciser pseudo, nom, prénom, ville
	 * Ne peut pas avoir le même pseudo qu'un user précedemment enregistré
	 * L'id (généré par la BDD) est renseigné automatiquement après l'ajout.
	 * @param l'user à ajouter
	 * @return L'id de l'user ajouté
	 * @throws DatabaseConstraintException si un evenement au même nom existe déjà
	 */
	public long createUser(UserEntity user) throws DatabaseConstraintException {
		try {
			em.persist(user);
			em.flush();
		} catch (PersistenceException e) {
			//TODO: je sais pas si l'exception est jamais lancée
			throw new DatabaseConstraintException();
		}
		return user.getId();
	}

	public void deleteUser(long id) throws Exception {
		UserEntity user = em.find(UserEntity.class,  id);
		if (user == null) throw new Exception();
		em.remove(em.merge(user));
	}

	
	public List<EventEntity> getIdOwnerEvents(long id) {
		List<EventEntity> returnValue = null;
		try {
			TypedQuery<EventEntity> query = null;
			query = em.createNamedQuery("FindEventsByIdOwner", EventEntity.class);
			query.setParameter("pidowner", id);
			returnValue = query.getResultList();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return returnValue;
	}

	public List<InterestEntity> getInterestById(String pseudo) {
		List<InterestEntity> returnValue = null;

		try {
			TypedQuery<InterestEntity> query = null;
			query = em.createNamedQuery("FindInterestsById", InterestEntity.class);
			query.setParameter("ppseudo", pseudo);
			returnValue = query.getResultList();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return returnValue;
	}

	//interets utilisateurs operations
	public String createInteret(InterestEntity interet) throws DatabaseConstraintException {
		try {
			em.persist(interet);
			em.flush();
		} catch (PersistenceException e) {
			//TODO: je sais pas si l'exception est jamais lancée
			throw new DatabaseConstraintException();
		}
		return interet.getpseudo();
	}

	public List<SeraPresentEntity> getSeraPresentById(long id) {
		List<SeraPresentEntity> returnValue = null;
		try {
			TypedQuery<SeraPresentEntity> query = null;
			query = em.createNamedQuery("FindSeraPresentById", SeraPresentEntity.class);
			query.setParameter("pid", id);
			returnValue = query.getResultList();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return returnValue;
	}

	public long createSeraPresent(SeraPresentEntity serapresent) throws DatabaseConstraintException {
		try {
			em.persist(serapresent);
			em.flush();
		} catch (PersistenceException e) {
			//TODO: je sais pas si l'exception est jamais lancée
			throw new DatabaseConstraintException();
		}
		return serapresent.getIdevent();
	}



}
