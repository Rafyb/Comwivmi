package comwivme;

import java.time.LocalDateTime;
import java.util.List;

import comwivme.dao.DataAccess;
import comwivme.dao.EventEntity;

public class ScheduledTasks implements Runnable {

	@Override
	public void run() {
		//deletePastEvents();
	}
	
	private void deletePastEvents() {
		DataAccess dataAccess = DataAccess.begin();
		List<EventEntity> li = dataAccess.getAllEvents();
		for(EventEntity e : li) {
			if(e.getDate().isBefore(LocalDateTime.now())) {
				try {
					dataAccess.deleteEvent(e.getId());
				} catch (Exception e1) {
					e1.printStackTrace();
				}
			}
		}
		dataAccess.closeConnection(true);
	}

}
