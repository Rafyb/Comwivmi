#Sprint 25/03  13h30 - 15h

Florentin PLAISANT : abs
Jimmy LETORET : supprimer une activité ( web )
Julien VANRYSSEL : inscription à un evenement et design ( mobile ) 
Manoel LECOCQ : Integration HTML/CSS sur le site
Nathan FORESTIER : Résolution de bug lier au serveur
Raphael BAUVIN : Lister les evenements sur le profil ( web )

##User story complété :
WEB
-> L'utilisateur peut visionner les evenements qu'il a créer et où il est inscrit
MOBILE
-> L'utilisateur peut s'inscrire à un evenement

##Bilan :
Le site prend forme et est déja plutot bien opérationnel 


##Objectif prochain sprint :
Lister les inscrits ( web ), supprimer activité ( web ), s'inscrire ( mobile )