package comwivme.dto;

public class InterestDto {
	
	protected String pseudo;
	protected String nom;
	
	public String getpseudo() {
		return pseudo;
	}
	public void setpseudo(String pseudo) {
		this.pseudo = pseudo;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}

}
