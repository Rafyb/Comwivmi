// @flow
import Page from './Page.js';
import $ from 'jquery';
import PageRenderer from '../PageRenderer.js';
import HomePage from './HomePage.js';

export default class loginPage extends Page {

	constructor(){
		super('.content');
		// $FlowFixMe
		this.submit = this.submit.bind(this);

		$(`.singinButton`).click(e =>{
			e.preventDefault();
			PageRenderer.renderPage(inscrire);
		});
		
	}

	render():string {
		return `
<div class="container-login">
<form class="loginPage">
		<label>
			Adresse mail :
			<input type="text" name="mail" class="form-control">
		</label>
		<label>
			Mot de passe :
			<input type="password" name="pwd" class="form-control">
		</label>
		<button type="submit" class="btn btn-default">Connecter</button>
    </form>
    <a href="#" class="singinButton">S'inscrire</a>
    </div>`
        ;
	}

	mount(container:HTMLElement):void {
		$('form.loginPage').submit( this.submit );
	}

	submit(event:Event):void {
		event.preventDefault();
		const fieldNames:Array<string> = [
			'mail',
			'pwd',
		];
		// on récupère la valeur saisie dans chaque champ
		const values:any = {};
		const errors:Array<string> = [];

		fieldNames.forEach( (fieldName:string) => {
			const value = this.getFieldValue(fieldName);
			if ( !value ){
				errors.push( `Le champ ${fieldName} ne peut être vide !` );
			}
			values[fieldName] = value;
		});
        //console.log(values.mail);
        //console.log(values.pwd);
		if (errors.length) {
			// si des erreurs sont détectées, on les affiche
			alert( errors.join('\n') );
		} else {
			// si il n'y a pas d'erreur on envoie les données
			const login = {
				mail: values.mail,
				password: values.pwd
            };
			console.log(login);
			console.log(JSON.stringify(login));
			fetch( '/comwivme/users/verif', {
					method:'POST',
					headers: { 'Content-Type': 'application/json' },
					body: JSON.stringify(login)
				})
			.then(response => {
				if (!response.ok) {
					throw new Error( `${response.status} : ${response.statusText}` );
				}
				// ici on recupere les infos
				return response.json();
			})
			.then ( user => {
				const json = (JSON.stringify(user)).split(':');

				PageRenderer.user.id = (json[1].split(','))[0];
				console.log(PageRenderer.user.id);
				PageRenderer.user.pseudo = (json[2].split(','))[0].replace("\"","").replace("\"","");
				console.log(PageRenderer.user.pseudo);
				PageRenderer.user.mail = (json[3].split(','))[0].replace("\"","").replace("\"","");
				console.log(PageRenderer.user.mail);
				PageRenderer.user.nom = (json[4].split(','))[0].replace("\"","").replace("\"","");
				console.log(PageRenderer.user.nom);
				PageRenderer.user.prenom = (json[5].split(','))[0].replace("\"","").replace("\"","");
				console.log(PageRenderer.user.prenom);
				PageRenderer.user.date = (json[6].split('T'))[0].replace("\"","");
				console.log(PageRenderer.user.date);
				PageRenderer.user.ville = (json[8].split(','))[0].replace("\"","").replace("\"","");
				console.log(PageRenderer.user.ville);
				PageRenderer.user.role = (json[9].split(','))[0].replace("\"","").replace("}","").replace("\"","");
				console.log(PageRenderer.user.role);
				alert(`Vous etes connecté avec succes`);

				// puis on redirige
				let home = new HomePage();
				PageRenderer.renderPage(home);
                $(`.eventListButton`).show();
				$(`.eventFormButton`).show();
				$(`.profilButton`).show();
				$(`.loginButton`).hide();
				
			
			})
			.catch( error => alert(`Connection impossible : ${error.message}`) );
		}
	}
    
	getFieldValue(fieldName:string):?string|Array<string>{
		// on récupère une référence vers le champ qui a comme attribut `name` la valeur fieldName (nom, base, prix_petite, etc.)
		const field:?HTMLElement = document.querySelector(`[name=${fieldName}]`);
		if ( field instanceof HTMLInputElement ) {
			// s'il s'agit d'un <input> on utilise la propriété `value`
			// et on retourne la chaine de caractère saisie
			return field.value != '' ? field.value : null;
		} else if ( field instanceof HTMLSelectElement ) {
			// s'il s'agit d'un <select> on utilise la propriété `selectedOptions`
			const values:Array<string> = [];
			for (let i = 0; i < field.selectedOptions.length; i++) {
				values.push( field.selectedOptions[i].value );
			}
			// et on retourne un tableau avec les valeurs sélectionnées
			return values.length ? values : null;
		}
		return null;
	}
}